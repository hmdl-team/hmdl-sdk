package sdk

import (
	"fmt"
	"net/http"
)

// IsSysError kiểm tra xem lỗi có phải là lỗi hệ thống
func IsSysError(err error) bool {
	e, ok := err.(*AppError)
	return ok && e.Type == SYSERROR
}

// IsPanic kiểm tra xem lỗi có phải là lỗi nghiêm trọng
func IsPanic(err error) bool {
	e, ok := err.(*AppError)
	return ok && e.Type == PANIC
}

// IsNotFoundError kiểm tra xem lỗi có phải là lỗi không tìm thấy
func IsNotFoundError(err error) bool {
	e, ok := err.(*AppError)
	return ok && e.StatusCode == http.StatusNotFound
}

func (err *AppError) WithType(errType ErrorType) *AppError {
	err.Type = errType
	return err
}

func (err *AppError) WithMessage(message string) *AppError {
	err.Message = message
	return err
}

func (err *AppError) WithApiPath(apiPath string) *AppError {
	err.Log = apiPath
	return err
}

func (err *AppError) WithCode(code int) *AppError {
	err.Code = code
	return err
}

func (err *AppError) WithModule(module string) *AppError {
	err.Module = module
	return err
}

func (err *AppError) WithStatusCode(statusCode int) *AppError {
	err.StatusCode = statusCode
	return err
}

func (err *AppError) WithInput(input interface{}) *AppError {
	err.Input = input
	return err
}

// WithData Thêm thông tin bổ xung vào lỗi để client có thể xử lý thêm
func (err *AppError) WithData(data map[string]interface{}) *AppError {
	err.Data = data
	return err
}

// WithRootError Thêm thông tin root Error
func (err *AppError) WithRootError(errRoot error) *AppError {
	err.RootError = errRoot
	return err
}

// WithErrorKey Thêm thông tin key Error
func (err *AppError) WithErrorKey(key string) *AppError {
	err.Key = key
	return err
}

func (err AppError) WithReason(reason string) *AppError {
	err.Module = reason
	return &err
}

func (err AppError) WithDetail(key string, detail interface{}) *AppError {
	if err.Details == nil {
		err.Details = map[string]interface{}{}
	}
	err.Details[key] = detail
	return &err
}

func (err AppError) WithReasonf(reason string, args ...interface{}) *AppError {
	return err.WithReason(fmt.Sprintf(reason, args...))
}

// CheckErrorProduction xử lý error nếu không debug thì không show log ra
func (err *AppError) CheckErrorProduction() *AppError {
	if IsProduction() {
		err.ErrorApp = map[string]interface{}{}
	}
	return err
}
