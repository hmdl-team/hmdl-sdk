package sdk

import (
	"database/sql/driver"
	"encoding/xml"
	"fmt"
	"github.com/jinzhu/now"
	"github.com/nleeper/goment"
	"github.com/spf13/cast"
	"strings"
	"time"
)

// Các định dạng thời gian
const (
	layoutDate             = "02-01-2006"
	layoutSQl              = "2006-01-02"
	layoutDateTime         = "02-01-2006 15:04:05"
	layoutDateIso          = "2006-01-02T15:04:05"
	layoutDateWithTimezone = "2006-01-02T15:04:05.999999-07:00"
	CustomDTLayout         = "2006-01-02 15:04:05"
	layoutDateWithUtc      = "2006-01-02 15:04:05 -0700 MST"
)

// MyDateTime GHNSQLTime
type MyDateTime struct {
	time.Time
}

// ConvertStringToTime chuyển đổi chuỗi thành giá trị thời gian
func ConvertStringToTime(value string) (*MyDateTime, error) {
	s := strings.Trim(value, "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateFormats := []string{
		layoutDate,
		layoutDateTime,
		CustomDTLayout,
		layoutDateIso,
		layoutDateWithTimezone,
		layoutDateWithUtc,
	}

	for _, format := range dateFormats {
		dateTime, err := time.Parse(format, s)
		if err == nil {
			return &MyDateTime{Time: dateTime}, nil
		}
	}

	return nil, fmt.Errorf("Không thể chuyển đổi thành thời gian: %s", value)
}

func ConvertStringToTimeWithLayer(value string, layer string) (*time.Time, error) {
	s := strings.Trim(value, "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateTime, errDateTime := time.Parse(layer, s)
	if errDateTime == nil {
		return &dateTime, nil
	}
	return nil, errDateTime
}

func NewMyDateTime(hour, min, sec int) MyDateTime {
	t := time.Date(0, time.January, 1, hour, min, sec, 0, time.UTC)
	return MyDateTime{
		Time: t,
	}
}
func NewMyDateTimeFromString(b interface{}) *MyDateTime {
	if cast.ToString(b) == "null" {
		return nil
	}
	dateTime, errDateIso := ConvertStringToTime(cast.ToString(b))
	if errDateIso != nil {
		return nil
	}
	return dateTime
}

func MyDateTimeNow() MyDateTime {
	return MyDateTime{
		Time: time.Now(),
	}
}
func MyDateTimeNowP() *MyDateTime {
	return &MyDateTime{
		Time: time.Now(),
	}
}

func (mt *MyDateTime) IsNil() bool {
	return mt.IsZero()
}

func (mt *MyDateTime) SetDateTimeNow() *MyDateTime {
	mt.Time = time.Now()
	return mt
}

func (mt *MyDateTime) EndOfMonth() MyDateTime {
	nowTime := now.New(*mt.ToTime()).EndOfMonth()
	return MyDateTime{
		Time: nowTime,
	}
}

func (mt *MyDateTime) String() string {
	return fmt.Sprintf("%v", mt.Format(CustomDTLayout))
}

func (mt *MyDateTime) Scan(value interface{}) error {
	if value == nil {
		mt.Time = time.Time{}
		return nil
	}

	switch v := value.(type) {
	case time.Time:
		mt.Time = v
	case []byte:
		t, err := time.Parse(CustomDTLayout, string(v))
		if err != nil {
			return err
		}
		mt.Time = t
	case string:
		t, err := time.Parse(CustomDTLayout, v)
		if err != nil {
			return err
		}
		mt.Time = t
	case nil:
		*mt = MyDateTime{}
	default:
		return fmt.Errorf("cannot sql.Scan() MyTime from: %#v", v)
	}
	return nil
}

func (mt MyDateTime) Value() (driver.Value, error) {
	return driver.Value(time.Time(mt.Time)), nil
}

func (mt *MyDateTime) UnmarshalText(value string) error {
	dd, err := time.Parse(layoutDateTime, value)
	if err != nil {
		return err
	}
	*mt = MyDateTime{
		Time: dd,
	}
	return nil
}

func (mt MyDateTime) GormDataType() string {
	return "DateTime"
}

func (mt *MyDateTime) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var s string
	err := d.DecodeElement(&s, &start)
	if err != nil {
		return err
	}

	return mt.UnmarshalJSON([]byte(s))
}

func (mt *MyDateTime) UnmarshalParam(param string) error {
	return mt.UnmarshalJSON([]byte(param))
}

func (mt *MyDateTime) UnmarshalJSON(b []byte) error {
	if string(b) == "null" {
		return nil
	}
	s := strings.Trim(string(b), `"`)
	s = strings.Trim(string(b), "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateTime, errDateIso := ConvertStringToTime(s)

	if errDateIso != nil {
		return errDateIso
	}
	var err error
	if dateTime != nil {
		mt.Time, err = time.Parse(CustomDTLayout, dateTime.Format(CustomDTLayout))
	}
	if err != nil {

	}
	return nil
}

// MarshalJSON Implement phương thức MarshalJSON() để định dạng đầu ra JSON theo đúng định dạng
func (mt MyDateTime) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%s"`, mt.Format(CustomDTLayout))), nil
}

func (mt *MyDateTime) ToTime() *time.Time {
	if mt == nil || mt.Time.IsZero() {
		return nil
	}
	return &mt.Time
}

func (mt *MyDateTime) ToSqlDateString() string {
	if mt == nil || mt.IsNil() {
		return ""
	}
	return mt.Format(layoutSQl)
}

func GetEndDayOfMonth(month int, year int) MyDateTime {
	time := time.Date(year, time.Month(month), 1, 1, 10, 30, 0, time.UTC)

	g, _ := goment.New(time)

	endDayOfmont := g.EndOf("M")

	return MyDateTime{endDayOfmont.ToTime()}
}

func GetTotalDayOfMonth(month int, year int) int {
	time := time.Date(year, time.Month(month), 1, 1, 10, 30, 0, time.UTC)

	g, _ := goment.New(time)

	endDayOfmont := g.EndOf("M")

	return endDayOfmont.Date()
}
