package sdk

import (
	"context"
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/spf13/cobra"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Component interface {
	ID() string
	InitFlags()
	Activate(ServiceContext) error
	Stop() error
}

type ServiceHandler func(svc ServiceContext) error
type CommandHandler func(svc ServiceContext, args []string)

type ServiceContext interface {
	Run()
	Load() error
	Stop() error
	Command(command string, handler CommandHandler) ServiceContext
	Logger(prefix string) LoggerInterface
	RestHandler(handler ServiceHandler) ServiceContext
	MustGet(id string) interface{}
	Get(id string) (interface{}, bool)
	EnvName() string
	GetName() string
	OutEnv()
	AddOptions(opts ...OptionSct) ServiceContext
}

type serviceCtx struct {
	name        string
	env         string
	server      *http.Server
	components  []Component
	store       map[string]Component
	rootCmd     *cobra.Command
	restHandler ServiceHandler
	cmdLine     *AppFlagSet
	logger      LoggerInterface
}

type OptionSct func(ctx *serviceCtx)

func WithName(name string) OptionSct {
	return func(s *serviceCtx) { s.name = name }
}

func WithServer(sv *http.Server) OptionSct {
	return func(s *serviceCtx) { s.server = sv }
}

func WithComponent(c Component) OptionSct {
	return func(s *serviceCtx) {
		if _, exists := s.store[c.ID()]; !exists {
			s.components = append(s.components, c)
			s.store[c.ID()] = c
		}
	}
}

func NewService(opts ...OptionSct) ServiceContext {
	LoadEnv()

	name := os.Getenv("NAME")
	if name == "" {
		name = "SERVICE-" + time.Now().Format("02012006150405")
	}

	sv := &serviceCtx{
		name:    name,
		store:   make(map[string]Component),
		rootCmd: &cobra.Command{},
	}

	server := &http.Server{
		Addr:         fmt.Sprintf(":%v", os.Getenv("PORT")),
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logger, err := NewLogRusLogger(&ConfigLogRus{
		DefaultLevel: "trace",
		BasePrefix:   "service",
	})
	if err != nil {
		log.Fatal(err)
	}

	sv.logger = logger.GetLogger(name)
	sv.server = server
	sv.components = []Component{_logRusCustom}

	// Apply options
	for _, opt := range opts {
		opt(sv)
	}

	sv.initFlags()
	sv.cmdLine = newFlagSet(sv.name, flag.CommandLine)
	sv.parseFlags()

	return sv
}

func (s *serviceCtx) AddOptions(opts ...OptionSct) ServiceContext {
	for _, opt := range opts {
		opt(s)
	}
	return s
}

func (s *serviceCtx) parseFlags() {
	envFile := os.Getenv("ENV_FILE")
	if envFile == "" {
		envFile = ".env"
	}

	if _, err := os.Stat(envFile); err == nil {
		if err := godotenv.Load(envFile); err != nil {
			log.Fatalf("Error loading env(%s): %s", envFile, err)
		}
	} else if envFile != ".env" {
		log.Fatalf("Env file not found: %s", envFile)
	}

	s.cmdLine.Parse([]string{})
}

func newFlagSet(name string, fs *flag.FlagSet) *AppFlagSet {
	fSet := &AppFlagSet{fs}
	fSet.Usage = flagCustomUsage(name, fSet)
	return fSet
}

func (s *serviceCtx) initFlags() {
	flag.StringVar(&s.env, "app-env", GetEnvironment(), "Env for service. Ex: dev | stg | prd")

	lo.ForEach(s.components, func(c Component, _ int) {
		c.InitFlags()
	})
}

func (s *serviceCtx) Stop() error {
	s.logger.Infoln("Stopping service context")
	for _, c := range s.components {
		if err := c.Stop(); err != nil {
			return err
		}
	}
	s.logger.Infoln("Service context stopped")
	return nil
}

func (s *serviceCtx) MustGet(id string) interface{} {
	c, ok := s.Get(id)
	if !ok {
		panic(fmt.Sprintf("Cannot find component: %s", id))
	}
	return c
}

func (s *serviceCtx) Get(id string) (interface{}, bool) {
	c, ok := s.store[id]
	return c, ok
}

func (s *serviceCtx) EnvName() string { return s.env }
func (s *serviceCtx) GetName() string { return s.name }
func (s *serviceCtx) OutEnv()         { s.cmdLine.GetSampleEnvs() }

func (s *serviceCtx) Load() error {
	s.logger.Infof("Service context is loading...")

	lo.ForEach(s.components, func(c Component, _ int) {
		if err := c.Activate(s); err != nil {
			s.logger.Error(err)
		}
	})

	s.Command("outenv", func(svc ServiceContext, _ []string) {
		svc.OutEnv()
	})

	s.logger.Infof("ENVIRONMENT: %s", os.Getenv("ENVIRONMENT"))
	return nil
}

func (s *serviceCtx) Logger(prefix string) LoggerInterface {
	return s.logger
}

func (s *serviceCtx) Command(command string, handler CommandHandler) ServiceContext {
	s.rootCmd.AddCommand(&cobra.Command{
		Use: command,
		Run: func(_ *cobra.Command, args []string) {
			handler(s, args)
		},
	})
	return s
}

func (s *serviceCtx) Run() {
	// Nạp handler
	if s.restHandler != nil {
		if err := s.restHandler(s); err != nil {
			s.logger.Error(err)
		}
	}

	s.rootCmd = &cobra.Command{
		Use:   "app",
		Short: "Run app",
		Run:   func(_ *cobra.Command, _ []string) { s.runServer() },
	}

	commands := []struct {
		use   string
		short string
		run   func()
	}{
		{"server", "Run server!", s.runServer},
		{"outenv", "Output all environment variables", s.OutEnv},
	}

	for _, cmd := range commands {
		s.rootCmd.AddCommand(&cobra.Command{
			Use:   cmd.use,
			Short: cmd.short,
			Run:   func(_ *cobra.Command, _ []string) { cmd.run() },
		})
	}

	if err := s.rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func (s *serviceCtx) runServer() {
	// Kiểm tra nếu có component "echo"
	if echoComponent, found := s.Get("echo"); found {
		if echoComp, ok := echoComponent.(ECHOComponent); ok {
			s.server = echoComp.GetHttpServer()
		} else {
			s.logger.Warn("Component 'echo' found but failed to cast to ECHOComponent")
		}
	}

	s.logger.Infof("Starting server on %s", s.server.Addr)
	done := make(chan bool, 1)

	go gracefulShutdown(s.server, done)

	// Lắng nghe lỗi từ server
	if err := s.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		s.logger.Fatal(err)
	}

	<-done
	s.logger.Info("Graceful shutdown complete.")
}

//func (s *serviceCtx) RestHandler(handler ServiceHandler) ServiceContext {
//	s.restHandler = handler
//	return s
//}

func gracefulShutdown(apiServer *http.Server, done chan bool) {
	// Create context that listens for the interrupt signal from the OS.
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	// Listen for the interrupt signal.
	<-ctx.Done()

	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := apiServer.Shutdown(ctx); err != nil {
		log.Printf("Server forced to shutdown with error: %v", err)
	}

	log.Println("Server exiting")

	// Notify the main goroutine that the shutdown is complete
	done <- true
}

func (s *serviceCtx) RestHandler(handler ServiceHandler) ServiceContext {
	s.restHandler = handler
	return s
}
