package helper

import (
	_const "gitlab.com/hmdl-team/hmdl-sdk/const"
	"strings"
	"time"
)

func ConvertStringToTimeWithLayer(value string, layer string) (*time.Time, error) {
	s := strings.Trim(value, "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateTime, errDateTime := time.Parse(layer, s)
	if errDateTime == nil {
		return &dateTime, nil
	}
	return nil, errDateTime
}

func ConvertStringToTime(value string) (*time.Time, error) {

	s := strings.Trim(value, "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateTime, errDateTime := time.Parse(_const.LayoutDate, s)
	if errDateTime == nil {
		return &dateTime, nil
	}

	dateTime, errDateTime = time.Parse(_const.LayoutDateTime, s)
	if errDateTime == nil {
		return &dateTime, nil
	}

	dateTime, errDateTime = time.Parse(_const.CustomDTLayout, s)
	if errDateTime == nil {
		return &dateTime, nil
	}

	dateTime, errDateTime = time.Parse(_const.LayoutDateIso, s)
	if errDateTime == nil {
		return &dateTime, nil
	}

	dateTime, errDateTime = time.Parse(_const.LayoutDateWithTimezone, s)
	if errDateTime == nil {
		return &dateTime, nil
	}

	return nil, errDateTime
}
