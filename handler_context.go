package sdk

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

// HandlerContext là struct bọc Context của Echo
type HandlerContext struct {
	echo.Context
}

// GetHandlerContext từ echo Context
func GetHandlerContext(c echo.Context) *HandlerContext {
	return c.(*HandlerContext)
}

// Trả về lỗi với mã lỗi và thông báo
func (s *HandlerContext) RespondError(err error, code int, defaultMessage, errorType string) error {
	if appErr, ok := err.(*AppError); ok {
		return s.JSON(code, appErr)
	}
	return s.JSON(code, NewAppError(err))
}

// Các phương thức xử lý lỗi
func (s *HandlerContext) BadRequest(err error) error {
	return s.RespondError(err, http.StatusBadRequest, "Bad Request", "BadRequest")
}

func (s *HandlerContext) Unauthorized(err error) error {
	return s.RespondError(err, http.StatusUnauthorized, "Unauthorized", "Unauthorized")
}

func (s *HandlerContext) NotFound(err error) error {
	return s.RespondError(err, http.StatusNotFound, "Not Found", "NotFound")
}

func (s *HandlerContext) Conflict(err error) error {
	return s.RespondError(err, http.StatusConflict, "Conflict Error", "ConflictError")
}

func (s *HandlerContext) InternalServerError(err error) error {
	if appErr, ok := err.(*AppError); ok {
		return appErr
	}
	return NewInternalError(err)
}

// Các phương thức xử lý phản hồi thành công
func (s *HandlerContext) Ok(data interface{}) error {
	return s.JSON(http.StatusOK, NewSuccessResponse(data))
}

func (s *HandlerContext) OkWithMessage(message string) error {
	return s.JSON(http.StatusOK, NewSuccessWithMessageResponse(message))
}

func (s *HandlerContext) OkWithMessageAndData(data interface{}, message string) error {
	return s.JSON(http.StatusOK, NewSuccessWithMessageResponseAndData(data, message))
}

func (s *HandlerContext) OkPaging(data interface{}, paging IPaging) error {
	return s.JSON(http.StatusOK, NewSuccessResponse(data).WithPaging(s.NewPaging(paging)))
}

// Xử lý phản hồi chung
func (s *HandlerContext) Send(response *Response) error {
	return s.JSON(response.StatusCode, response)
}

// Xử lý lỗi chung
func (s *HandlerContext) HandleError(err error) error {
	if _, ok := err.(*AppError); ok {
		return err
	}
	if _, ok := err.(*Response); ok {
		return err
	}
	return s.InternalServerError(err)
}

// Lấy thông tin user sau đăng nhập
func (s *HandlerContext) GetUser() IUser {
	if user, ok := s.Get("user").(IUser); ok {
		return user
	}
	return nil
}

func (s *HandlerContext) GetUserUid() string {
	if user := s.GetUser(); user != nil {
		return user.GetUid()
	}
	return ""
}

func (s *HandlerContext) GetUserRole() string {
	if user := s.GetUser(); user != nil {
		return user.GetRole()
	}
	return ""
}

// Tạo đối tượng phân trang
func (s *HandlerContext) NewPaging(paging IPaging) *Paging {
	perPage := paging.PerPage()
	totalItems := paging.Total()
	totalPages := (totalItems + perPage - 1) / perPage

	page, _ := strconv.Atoi(s.QueryParam("page"))
	if page < 1 {
		page = 1
	} else if page > totalPages {
		page = totalPages
	}

	hasNextPages := page < totalPages
	hasPreviousPages := page > 1
	nextPageUrl, prevPageUrl := s.buildPagingUrls(page, totalPages)

	first := (page-1)*perPage + 1
	last := page * perPage
	if last > totalItems {
		last = totalItems
	}

	if paging.IsGetAll() {
		perPage = totalItems
		totalPages = 1
		hasNextPages, hasPreviousPages = false, false
		first, last = 1, totalItems
		nextPageUrl, prevPageUrl = "", ""
	}

	return &Paging{
		CurrentPage:      page,
		LastPage:         totalPages,
		PerPage:          perPage,
		HasNextPages:     hasNextPages,
		HasPreviousPages: hasPreviousPages,
		NextPageUrl:      nextPageUrl,
		PreviousPageUrl:  prevPageUrl,
		First:            first,
		Last:             last,
		Total:            totalItems,
		GetAll:           paging.IsGetAll(),
	}
}

func (s *HandlerContext) buildPagingUrls(page, totalPages int) (string, string) {
	url := s.Request().URL
	query := url.Query()

	query.Set("page", strconv.Itoa(page+1))
	url.RawQuery = query.Encode()
	nextPageUrl := url.String()

	query.Set("page", strconv.Itoa(page-1))
	url.RawQuery = query.Encode()
	prevPageUrl := url.String()

	return nextPageUrl, prevPageUrl
}
