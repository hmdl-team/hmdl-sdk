package sdk

import (
	"encoding/json"
	"github.com/nats-io/nats.go"
	"net/http"
)

type NatMessage struct {
	*nats.Msg
}

// NatSender constructor for NatMessage
func NatSender(msg *nats.Msg) *NatMessage {
	return &NatMessage{Msg: msg}
}

// Bind Lấy giá trị đầu vào vPt là pointer của biến đầu vào sẽ được xuất ra
func (s *NatMessage) Bind(vPt interface{}) error {
	err := json.Unmarshal(s.Data, vPt)
	if err != nil {

		errResponse := NewAppError(err)
		if err := s.Respond(errResponse.ToBytes()); err != nil {
			return err
		}
		return errResponse
	}
	return nil
}

// HandleError Hàm xử lý error và respond về kết quả
func (s *NatMessage) HandleError(err error) {
	if err, ok := err.(*AppError); ok {
		errResponse := NewErrorResponse(err, "Error Bind", err.Error(), "ERROR_BIND")
		s.Respond(errResponse.ToBytes())
	}

}

// Ok Hàm respond kết quả success về cho client thường sử dụng cho request-reply
// [data] data của kết quả trả về sẽ được bọc trong lớp response trước khi trả về.
func (s *NatMessage) Ok(data interface{}) {
	s.Respond(NewSuccessResponse(data).ToBytes())
}

// BadRequest Hàm respond bad request về cho client thường sử dụng cho request-reply
func (s *NatMessage) BadRequest(err error) {
	errResponse := NewFullErrorResponse(http.StatusBadRequest, err, "Bad Request", err.Error(), "BAD_REQUEST", WARNING)
	s.Respond(errResponse.ToBytes())
}

// Conflict Hàm respond conflict request về cho client thường sử dụng cho request-reply
func (s *NatMessage) Conflict(err error) {
	errResponse := NewFullErrorResponse(http.StatusConflict, err, "Conflict", err.Error(), "CONFLICT", ERROR)
	s.Respond(errResponse.ToBytes())
}

// NotFound Hàm respond not found request về cho client thường sử dụng cho request-reply
func (s *NatMessage) NotFound(err error) {
	errResponse := NewFullErrorResponse(http.StatusNotFound, err, "Not Found", err.Error(), "NOT_FOUND", WARNING)
	s.Respond(errResponse.ToBytes())
}

// Unauthorized Hàm respond unauthorized request về cho client thường sử dụng cho request-reply
func (s *NatMessage) Unauthorized(err error) {
	errResponse := NewFullErrorResponse(http.StatusUnauthorized, err, "Unauthorized", err.Error(), "UNAUTHORIZED", ERROR)
	s.Respond(errResponse.ToBytes())
}

// InternalServerError Hàm respond internal server error request về cho client thường sử dụng cho request-reply
func (s *NatMessage) InternalServerError(err error) {
	errResponse := NewFullErrorResponse(http.StatusInternalServerError, err, "Internal Server Error", err.Error(), "INTERNAL_SERVER_ERROR", SYSERROR)
	s.Respond(errResponse.ToBytes())
}
