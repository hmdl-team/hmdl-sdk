package main

import (
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	"gitlab.com/hmdl-team/hmdl-sdk/component/echoc"
	"gitlab.com/hmdl-team/hmdl-sdk/component/gormc"
	"gitlab.com/hmdl-team/hmdl-sdk/component/kongc"
	"gitlab.com/hmdl-team/hmdl-sdk/component/natsc"
	"simple/router"
)

func main() {
	serviceCtx := sdk.NewService(
		sdk.WithName("test-service"),
		sdk.WithComponent(echoc.NewEcho("echo")),
		sdk.WithComponent(kongc.NewKong("kong")),
		sdk.WithComponent(natsc.NewNatsc("nats")),
		sdk.WithComponent(gormc.NewGormDB("AUTH", "AUTH")),
	)

	serviceCtx.RestHandler(router.NewRouter)

	if err := serviceCtx.Load(); err != nil {
		serviceCtx.Logger("service").Error(err)
	}

	sdk.Logger().WithFields(map[string]interface{}{
		"animal": "walrus",
		"size":   10,
	}).Info("A group of walrus emerges from the ocean")

	sdk.Logger().Info("Info log example")
	sdk.Logger().Warn("Warn log example")
	sdk.Logger().Error("Error log example")
	sdk.Logger().Debug("Debug log example")

	//logger := sdk.GlobalLogger().GetLogger("hmdl-test")

	//logger.Error("Test error : ", time.Now())

	//sdk.Logger().Error(sdk.NewError("T1").WithData(map[string]interface{}{
	//	"01": "test",
	//}))

	serviceCtx.Run()
}
