package router

import (
	"github.com/labstack/echo/v4"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	"gorm.io/gorm"
	"simple/services"
)

const compId = "echo"

type EchoComponent interface {
	GetPort() int
	GetRouter() *echo.Echo
}

type GormComponent interface {
	GetDB() *gorm.DB
}

func NewRouter(serviceCtx sdk.ServiceContext) error {
	comp := serviceCtx.MustGet(compId).(EchoComponent)

	e := comp.GetRouter()

	gormC := serviceCtx.MustGet("AUTH").(GormComponent)

	db := gormC.GetDB()

	sv := services.NewSQLRepository(db)

	e.GET("/getTime", func(c echo.Context) error {
		cc := sdk.GetHandlerContext(c)

		now, err := sv.GetDateTimeNow(cc.Request().Context())
		if err != nil {
			//sdk.Logger().Error(err)
			return cc.HandleError(err)
		}
		return cc.Ok(now)
	})

	e.GET("/check", func(c echo.Context) error {
		cc := sdk.GetHandlerContext(c)

		//err := sdk.NewAppError(nil).WithMessage("Check Lỗi!").WithCode(404)
		err := sdk.NewUnauthorizedErr(nil)
		if err != nil {
			return cc.HandleError(err)
		}
		return cc.OkWithMessage("Thành công!")
	})

	//server := &http.Server{
	//	Addr:         fmt.Sprintf(":%v", port),
	//	Handler:      e,
	//	IdleTimeout:  time.Minute,
	//	ReadTimeout:  10 * time.Second,
	//	WriteTimeout: 30 * time.Second,
	//}

	//serviceCtx.AddOptions(
	//	sdk.WithServer(server),
	//)

	return nil
}
