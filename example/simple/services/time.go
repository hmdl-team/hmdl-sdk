package services

import (
	"context"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"time"
)

type mysqlRepo struct {
	db *gorm.DB
}

func NewSQLRepository(db *gorm.DB) *mysqlRepo {
	return &mysqlRepo{db: db}
}

func (repo *mysqlRepo) GetDateTimeNow(ctx context.Context) (time.Time, error) {
	timeNow := time.Time{}
	if err := repo.db.Raw("select GetDate()").Scan(&timeNow).Error; err != nil {
		return timeNow, errors.WithStack(err)
	}

	return timeNow, nil
}
