package sdk

import (
	"github.com/labstack/echo/v4"
	"github.com/nats-io/nats.go"
	"gorm.io/gorm"
	"net/http"
)

type BaseIntf interface {
	ID() string
	Activate(sv ServiceContext) error
	Stop() error
	InitFlags()
}

type ECHOComponent interface {
	BaseIntf
	GetPort() int
	GetRouter() *echo.Echo
	GetHttpServer() *http.Server
}

type NATSComponent interface {
	BaseIntf
	GetNats() *nats.Conn
	GetNatJson() *nats.EncodedConn
}
type GormComponent interface {
	BaseIntf
	GetDB() *gorm.DB
}
