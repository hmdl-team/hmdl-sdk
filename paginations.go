package sdk

import (
	"fmt"
	"github.com/labstack/echo/v4"

	"strconv"
	"strings"
)

type Search struct {
	Column    string `json:"column"`
	Action    string `json:"action"`
	Query     string `json:"query"`
	Condition string `json:"condition"`
}

type Pagination struct {
	Limit        int64       `json:"limit"`
	Page         int64       `json:"page"`
	Sort         string      `json:"sort"`
	TotalRows    int64       `json:"total_rows"`
	FirstPage    string      `json:"first_page"`
	PreviousPage string      `json:"previous_page"`
	TotalPage    int64       `json:"total_page"`
	NextPage     string      `json:"next_page"`
	LastPage     string      `json:"last_page"`
	FromRow      int64       `json:"from_row"`
	ToRow        int64       `json:"to_row"`
	Rows         interface{} `json:"-"`
	Searches     []Search    `json:"searches"`
}

type RepositoryResult struct {
	Result    interface{}
	TotalPage int64 `json:"total_page"`
	Error     error
}

func GeneratePaginationRequest(context echo.Context) *Pagination {
	// default limit, page & sort parameter
	var limit, page int64
	limit = 100
	page = 0
	sort := "CreatedAt desc"
	condition := "where"

	var searchs []Search

	query := context.Request().URL.Query()

	for key, value := range query {
		queryValue := value[len(value)-1]

		switch key {
		case "limit":
			limit, _ = strconv.ParseInt(queryValue, 10, 64)
			break
		case "page":
			page, _ = strconv.ParseInt(queryValue, 10, 64)
			break
		case "sort":
			sort = queryValue
			break
		}

		// check if query parameter key contains dot
		if strings.Contains(key, ".") {
			// split query parameter key by dot
			searchKeys := strings.Split(key, ".")

			if len(searchKeys) > 2 {
				condition = searchKeys[2]
			}

			// create search object
			search := Search{
				Column:    searchKeys[0],
				Action:    searchKeys[1],
				Query:     queryValue,
				Condition: condition,
			}

			// add search object to searchs array
			searchs = append(searchs, search)
		}
	}

	return &Pagination{Limit: limit, Page: page, Sort: sort, Searches: searchs}
}

func XuLyThongTinPage(operationResult RepositoryResult, urlPath string, pagination Pagination) *Pagination {

	totalPages := operationResult.TotalPage

	var data = operationResult.Result.(*Pagination)

	// search query params
	searchQueryParams := ""

	for _, search := range pagination.Searches {
		searchQueryParams += fmt.Sprintf("&%s.%s=%s", search.Column, search.Action, search.Query)
	}

	// set first & last page pagination response
	data.FirstPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, 1, pagination.Sort) + searchQueryParams
	data.LastPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, totalPages, pagination.Sort) + searchQueryParams

	if data.Page > 1 {
		// set previous page pagination response
		data.PreviousPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, data.Page+1, pagination.Sort) + searchQueryParams
	}

	if data.Page < totalPages {
		// set next page pagination response
		data.NextPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, data.Page, pagination.Sort) + searchQueryParams
	}

	if data.Page > totalPages {
		// reset previous page
		data.PreviousPage = ""
	}
	return data
}
