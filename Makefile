ver = 1.0.25

git-update:
	git add . && git commit -m "update: sdk v$(ver)" && git push origin master
#update:
#	git add . && git commit -m "update: sdk v$(ver)" && git tag -a v$(ver) -m "update: sdk v$(ver)" && git push -q origin master --tags
tag:
	git describe --tags
git-clean:
	git rm -r --cached . && git add . && git commit -m ".gitignore is now working"
clean-tag:
	git tag | xargs git tag -d && git ls-remote --tags --refs origin | cut -f2 | xargs git push origin --delete
update-pkg-cache:
	GOPROXY=https://proxy.golang.org GO111MODULE=on \
	go get github.com/$(USER)/$(PACKAGE)@v$(VERSION)
git:
	git add . && git commit -m "update: sdk v$(ver)" && git push origin master