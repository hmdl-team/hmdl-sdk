package sdk

// interface định nghĩa cho các Request Input phải chứa các hàm này
type IPaging interface {
	PerPage() int
	Offset() int
	Total() int
	IsGetAll() bool
}

type Paging struct {
	CurrentPage      int    `json:"current_page"`
	LastPage         int    `json:"last_page"`
	PerPage          int    `json:"per_page"`
	HasNextPages     bool   `json:"has_next_pages"`
	HasPreviousPages bool   `json:"has_previous_pages"`
	NextPageUrl      string `json:"next_page_url"`
	PreviousPageUrl  string `json:"previous_page_url"`
	First            int    `json:"first"`
	Last             int    `json:"last"`
	Total            int    `json:"total"`
	GetAll           bool   `json:"get_all"`
}
