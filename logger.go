package sdk

import "gitlab.com/hmdl-team/hmdl-sdk/telegram_hook"

type Fields map[string]interface{}

type LoggerInterface interface {
	Print(args ...interface{})
	Debug(...interface{})
	Debugln(...interface{})
	Debugf(string, ...interface{})

	Info(...interface{})
	Infoln(...interface{})
	Infof(string, ...interface{})

	Warn(...interface{})
	Warnln(...interface{})
	Warnf(string, ...interface{})

	Error(...interface{})
	Errorln(...interface{})
	Errorf(string, ...interface{})

	Fatal(...interface{})
	Fatalln(...interface{})
	Fatalf(string, ...interface{})

	Panic(...interface{})
	Panicln(...interface{})
	Panicf(string, ...interface{})

	With(key string, value interface{}) LoggerInterface
	Withs(Fields) LoggerInterface
	WithFields(fields map[string]interface{}) LoggerInterface
	// add source field to log
	WithSrc() LoggerInterface
	WithError(err error) LoggerInterface
	WithField(key string, value interface{}) LoggerInterface
	GetLevel() string
	SendErrorChatTelegram(err error, config ...telegram_hook.Config)
	SendMessageChatTelegram(message string, config ...telegram_hook.Config)
}

var (
	//defaultLogger = newAppLogger(&Config{
	//	BasePrefix:   "core",
	//	DefaultLevel: "trace",
	//})

	_logRusCustom, _ = NewLogRusLogger(&ConfigLogRus{
		BasePrefix:   "core",
		DefaultLevel: "trace",
	})
)

type AppLogger interface {
	GetLogger(prefix string) LoggerInterface
}

func GlobalLogger() AppLogger {
	return _logRusCustom
}

func Logger() LoggerInterface {
	return _logRusCustom
}
