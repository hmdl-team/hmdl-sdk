package _const

const LayoutDate = "02-01-2006"
const LayoutSQl = "2006-01-02"

const LayoutDateTime = "02-01-2006 15:04:05"
const LayoutDateIso = "2006-01-02T15:04:05"
const LayoutDateWithTimezone = "2006-01-02T15:04:05.999999-07:00"

const CustomDTLayout = "2006-01-02 15:04:05"
