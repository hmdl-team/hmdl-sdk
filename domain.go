package sdk

import (
	"errors"
	"fmt"
	"github.com/go-ldap/ldap/v3"
	auth "github.com/korylprince/go-ad-auth/v3"
	"os"
	"time"
)

type UserDomain struct {
	UserName   string
	Name       string
	Email      string
	Title      string
	Company    string
	Department string
	Mobile     string
}

type Result struct {
	User  UserDomain
	Error error
}

//login
func loginDomain(userName, password string) (*UserDomain, error) {

	port, _ := StringToInt(os.Getenv("DOMAIN_PORT"))

	config := &auth.Config{
		Server: os.Getenv("DOMAIN_SERVER"),
		BaseDN: os.Getenv("DOMAIN_BASEDN"),
		Port:   port,
	}

	status, entry, _, err := auth.AuthenticateExtended(config, userName, password, nil, nil)

	if err != nil {
		//handle err
		return nil, err
	}

	if !status {
		//handle failed authentication
		return nil, errors.New("handle failed authentication")
	}

	name := entry.GetAttributeValue("name")
	mail := entry.GetAttributeValue("mail")
	title := entry.GetAttributeValue("title")
	department := entry.GetAttributeValue("department")
	company := entry.GetAttributeValue("company")
	mobile := entry.GetAttributeValue("mobile")

	return &UserDomain{
		UserName:   userName,
		Email:      mail,
		Title:      title,
		Department: department,
		Company:    company,
		Name:       name,
		Mobile:     mobile,
	}, nil

}

func GetUserDomain(userName, password string) (*UserDomain, error) {

	c1 := make(chan *Result, 1)

	go func() {
		userInfo, err := loginDomain(userName, password)

		result := Result{
			Error: err,
		}

		if userInfo != nil {
			result.User = *userInfo
		}

		c1 <- &result

	}()

	select {
	case result := <-c1:

		if result.Error != nil {
			return nil, result.Error
		}
		return &result.User, nil

	case <-time.After(3 * time.Second):
		return nil, errors.New("fail connect to ldap")
	}

}

func CheckUserDomainExiting(userName string) (*UserDomain, error) {
	// Initialize a connection and authenticate
	url := fmt.Sprintf("%s:%s", os.Getenv("DOMAIN_SERVER"), os.Getenv("DOMAIN_PORT"))
	conn, err := ldap.Dial("tcp", url)
	if err != nil {
		return nil, err
	}
	if err = conn.Bind(os.Getenv("DOMAIN_USER"), os.Getenv("DOMAIN_PASS")); err != nil {
		return nil, err
	}

	filterUrl := fmt.Sprintf("(&(objectCategory=person)(objectClass=user)(!sAMAccountType=805306370)(sAMAccountName=%s))", userName)

	// Search for the user you're looking for to obtain the distinguishedName and userAccountControl
	result, err := conn.Search(&ldap.SearchRequest{
		BaseDN:       os.Getenv("DOMAIN_BASEDN"),
		Scope:        ldap.ScopeWholeSubtree,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       filterUrl,
	})
	if err != nil {
		return nil, err
	}
	if len(result.Entries) < 1 {
		return nil, nil
	}

	user := result.Entries[0]

	name := user.GetAttributeValue("name")
	mail := user.GetAttributeValue("mail")
	title := user.GetAttributeValue("title")
	department := user.GetAttributeValue("department")
	company := user.GetAttributeValue("company")
	mobile := user.GetAttributeValue("mobile")

	return &UserDomain{
		UserName:   userName,
		Email:      mail,
		Title:      title,
		Department: department,
		Company:    company,
		Name:       name,
		Mobile:     mobile,
	}, nil
}
