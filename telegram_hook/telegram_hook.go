package telegram_hook

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"html"
	"net/http"
	"os"
	"strings"
	"time"
)

// TelegramHook to send logs via the Telegram API.
type TelegramHook struct {
	AppName     string
	icon        string
	c           *http.Client
	authToken   string
	targetID    string
	threadId    int
	apiEndpoint string
	async       bool
}

// apiRequest encapsulates the request structure we are sending to the
// Telegram API.
type apiRequest struct {
	ChatID    string `json:"chat_id"`
	ThreadId  int    `json:"message_thread_id"`
	Text      string `json:"text"`
	ParseMode string `json:"parse_mode,omitempty"`
}

// apiResponse encapsulates the response structure received from the
// Telegram API.
type apiResponse struct {
	Ok         bool                    `json:"ok"`
	ErrorCode  *int                    `json:"error_code,omitempty"`
	Desc       *string                 `json:"description,omitempty"`
	Result     *interface{}            `json:"result,omitempty"`
	Parameters *map[string]interface{} `json:"parameters"`
}

// Config defines a method for additional configuration when instantiating TelegramHook
type Config func(*TelegramHook)

// WithAsync Async sets logging to telegram as asynchronous
func WithAsync(b bool) Config {
	return func(hook *TelegramHook) {
		hook.async = b
	}
}

// WithTimeout Timeout sets http call timeout for telegram client
func WithTimeout(t time.Duration) Config {
	return func(hook *TelegramHook) {
		if t > 0 {
			hook.c.Timeout = t
		}
	}
}

func WithThreadId(threadId int) Config {
	return func(hook *TelegramHook) {
		if threadId > 0 || threadId < 0 {
			hook.threadId = threadId
		}
	}
}

func WithChatId(chatId string) Config {
	return func(hook *TelegramHook) {
		hook.targetID = chatId
	}
}

func WithIcon(icon string) Config {
	return func(hook *TelegramHook) {
		if hook != nil && len(icon) > 0 {
			hook.icon = icon
		}
	}
}

func NewTelegramHook(appName, authToken, targetID string, threadId int, config ...Config) (*TelegramHook, error) {
	client := &http.Client{}
	return NewTelegramHookWithClient(appName, authToken, targetID, threadId, client, config...)
}

func NewTelegramHookWithClient(appName, authToken, targetID string, threadId int, client *http.Client, configs ...Config) (*TelegramHook, error) {
	apiEndpoint := fmt.Sprintf("https://api.telegram.org/bot%s", authToken)
	h := TelegramHook{
		AppName:     appName,
		c:           client,
		authToken:   authToken,
		targetID:    targetID,
		threadId:    threadId,
		apiEndpoint: apiEndpoint,
		async:       false,
	}

	for _, c := range configs {
		c(&h)
	}

	// Verify the API token is valid and correct before continuing
	err := h.verifyToken()
	if err != nil {
		return nil, err
	}

	return &h, nil
}

// verifyToken issues a test request to the Telegram API to ensure the
// provided token is correct and valid.
func (hook *TelegramHook) verifyToken() error {
	endpoint := strings.Join([]string{hook.apiEndpoint, "getme"}, "/")
	res, err := hook.c.Get(endpoint)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	apiRes := apiResponse{}
	err = json.NewDecoder(res.Body).Decode(&apiRes)
	if err != nil {
		return err
	}

	if !apiRes.Ok {
		// Received an error from the Telegram API
		msg := "Received error response from Telegram API"
		if apiRes.ErrorCode != nil {
			msg = fmt.Sprintf("%s (error code %d)", msg, *apiRes.ErrorCode)
		}
		if apiRes.Desc != nil {
			msg = fmt.Sprintf("%s: %s", msg, *apiRes.Desc)
		}
		j, _ := json.MarshalIndent(apiRes, "", "\t")
		msg = fmt.Sprintf("%s\n%s", msg, j)
		return fmt.Errorf(msg)
	}

	return nil
}

func (hook *TelegramHook) getIconHtml() string {
	icon := ""
	switch hook.icon {
	case "clear":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">❌</tg-emoji>`, time.Now().Unix())
	case "message":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">⬇️</tg-emoji>`, time.Now().Unix())
	case "ok":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">✅</tg-emoji>`, time.Now().Unix())
	case "panic", "fatal":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">‼️</tg-emoji>`, time.Now().Unix())
	case "warn", "warning", "error":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">❗</tg-emoji>`, time.Now().Unix())
	case "info":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">⭐</tg-emoji>`, time.Now().Unix())
	case "debug":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">❇️ </tg-emoji>`, time.Now().Unix())
	case "trace":
		icon = fmt.Sprintf(`<tg-emoji emoji-id="%v">⬇️</tg-emoji>`, time.Now().Unix())
	}

	return icon
}

// SendMessage  issues the provided message to the Telegram API.
func (hook *TelegramHook) SendMessage(msg string, configs ...Config) error {
	for _, c := range configs {
		c(hook)
	}

	icon := ""

	if len(hook.icon) > 0 {
		icon = hook.getIconHtml()
	}
	apiReq := apiRequest{
		ChatID:    hook.targetID,
		ThreadId:  hook.threadId,
		Text:      fmt.Sprintf("%v %v : %v", icon, hook.AppName, msg),
		ParseMode: "HTML",
	}

	b, err := json.Marshal(apiReq)
	if err != nil {
		return err
	}

	res, err := hook.c.Post(strings.Join([]string{hook.apiEndpoint, "sendmessage"}, "/"), "application/json", bytes.NewReader(b))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Encountered error when issuing request to Telegram API, %v", err)
		return err
	}
	defer res.Body.Close()

	apiRes := apiResponse{}
	err = json.NewDecoder(res.Body).Decode(&apiRes)
	if err != nil {
		return err
	}

	if !apiRes.Ok {
		// Received an error from the Telegram API
		msg := "Received error response from Telegram API"
		if apiRes.ErrorCode != nil {
			msg = fmt.Sprintf("%s (error code %d)", msg, *apiRes.ErrorCode)
		}
		if apiRes.Desc != nil {
			msg = fmt.Sprintf("%s: %s; %s", msg, *apiRes.Desc, apiRes.Parameters)
		}
		fmt.Println(msg)
		return fmt.Errorf(msg)
	}

	return nil
}

// SendMessageWithChatId sendMessage issues the provided message to the Telegram API.
func (hook *TelegramHook) SendMessageWithChatId(msg string, chatId string, configs ...Config) error {
	for _, c := range configs {
		c(hook)
	}

	icon := ""

	if len(hook.icon) > 0 {
		icon = hook.getIconHtml()
	}

	apiReq := apiRequest{
		ChatID:    chatId,
		Text:      fmt.Sprintf("%v %v : %v", icon, hook.AppName, msg),
		ParseMode: "HTML",
		ThreadId:  hook.threadId,
	}

	b, err := json.Marshal(apiReq)
	if err != nil {
		return err
	}

	res, err := hook.c.Post(strings.Join([]string{hook.apiEndpoint, "sendmessage"}, "/"), "application/json", bytes.NewReader(b))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Encountered error when issuing request to Telegram API, %v", err)
		return err
	}
	defer res.Body.Close()

	apiRes := apiResponse{}
	err = json.NewDecoder(res.Body).Decode(&apiRes)
	if err != nil {
		return err
	}

	if !apiRes.Ok {
		// Received an error from the Telegram API
		msg := "Received error response from Telegram API"
		if apiRes.ErrorCode != nil {
			msg = fmt.Sprintf("%s (error code %d)", msg, *apiRes.ErrorCode)
		}
		if apiRes.Desc != nil {
			msg = fmt.Sprintf("%s: %s", msg, *apiRes.Desc)
		}
		return fmt.Errorf(msg)
	}

	return nil
}

// createMessage crafts an HTML-formatted message to send to the
// Telegram API.
func (hook *TelegramHook) createMessage(entry *logrus.Entry) string {
	var msg string

	switch entry.Level {
	case logrus.PanicLevel:
		msg = "<b>PANIC</b>"
	case logrus.FatalLevel:
		msg = "<b>FATAL</b>"
	case logrus.ErrorLevel:
		msg = "<b>ERROR</b>"
	}

	//msg = strings.Join([]string{msg, hook.AppName}, "@")
	msg = strings.Join([]string{msg, entry.Message}, " : ")
	if len(entry.Data) > 0 {
		msg = strings.Join([]string{msg, "<pre>"}, "\n")
		for k, v := range entry.Data {
			msg = strings.Join([]string{msg, html.EscapeString(fmt.Sprintf("\t%s: %+v", k, v))}, "\n")
		}
		msg = strings.Join([]string{msg, "</pre>"}, "\n")
	}
	return msg
}

// Fire emits a log message to the Telegram API.
func (hook *TelegramHook) Fire(entry *logrus.Entry) error {
	msg := hook.createMessage(entry)

	if hook.async {
		go hook.SendMessage(msg, WithIcon(entry.Level.String()))
		return nil
	}

	err := hook.SendMessage(msg, WithIcon(entry.Level.String()))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to send message, %v", err)
		return err
	}

	return nil
}

// Levels returns the log levels that the hook should be enabled for.
func (hook *TelegramHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.ErrorLevel,
		logrus.FatalLevel,
		logrus.PanicLevel,
	}
}
