package telegram_hook_test

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/hmdl-team/hmdl-sdk/telegram_hook"
	"testing"
)

func TestNewHook(t *testing.T) {
	h, err := telegram_hook.NewTelegramHook("test", "", "")
	if err != nil {
		t.Fatalf("Error on valid Telegram API token and target: %s", err)
	}
	log.AddHook(h)

	log.WithError(errors.New("an error")).
		WithFields(log.Fields{
			"animal": "walrus",
			"number": 1,
			"size":   10,
			"html":   "<b>bold</b>",
		}).Errorf("A walrus appears")
}
