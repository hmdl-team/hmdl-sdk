package sdk

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	logrus2 "gitlab.com/hmdl-team/hmdl-sdk/component/logrus"
	"gitlab.com/hmdl-team/hmdl-sdk/telegram_hook"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

type loggerLogrus struct {
	logger       *logrus.Logger
	TelegramHook *telegram_hook.TelegramHook
	cfg          *ConfigLogRus
	logLevel     string
	logToFile    bool
	logPath      string
}

type logger struct {
	entry *logrus.Entry
}

func (al *loggerLogrus) ID() string {
	return fmt.Sprintf("%s.logger", al.cfg.BasePrefix)
}

func (al *loggerLogrus) InitFlags() {

	if al != nil {
		// Thiết lập các cờ khởi tạo, ví dụ cấp độ log
		flag.StringVar(&al.logLevel, "LOG_LEVEL", al.cfg.DefaultLevel, "LOG_LEVEL: panic | fatal | error | warn | info | debug | trace")
		flag.StringVar(&al.logPath, "LOG_PATH", `.\LOGS`, `LOG_PATH default: .\LOGS `)
		flag.BoolVar(&al.logToFile, "LOG_FILE", false, "LOG_FILE default: false")
	}

}

func (al *loggerLogrus) Activate(context ServiceContext) error {
	// Áp dụng cấu hình cấp độ log
	level, err := logrus.ParseLevel(al.logLevel)
	if err != nil {
		return err
	}
	al.logger.SetLevel(level)
	return nil
}

func (al *loggerLogrus) Stop() error {
	// Dừng các hoạt động (nếu cần)
	al.logger.Info("Logger stopped")
	return nil
}

func (al *loggerLogrus) GetLogger(prefix string) LoggerInterface {
	return al
}

var (
	loggerInstance *loggerLogrus
	once           sync.Once
)

type logrusFormatter struct {
	logrus.TextFormatter
}

func (f *logrusFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	var strData string
	keys := []string{"Type", "Time", "Message"}
	mes := map[string]interface{}{
		"Type":    strings.ToUpper(entry.Level.String()),
		"Time":    entry.Time.Format(f.TimestampFormat),
		"Message": entry.Message,
	}

	for key, value := range entry.Data {
		if value != nil && len(cast.ToString(value)) > 0 {
			mes[key] = value
			keys = append(keys, key)
		}
	}

	for _, key := range keys {
		if value, exists := mes[key]; exists {
			strData += fmt.Sprintf("- %s: %v\n", key, value)
		}
	}

	strData = strData + "-------------\n"

	message := []byte(strData)
	return message, nil
}

// ConfigLogRus defines the logging configuration
type ConfigLogRus struct {
	DefaultLevel string
	BasePrefix   string
}

// NewLogRusLogger creates a new logger instance
func NewLogRusLogger(config *ConfigLogRus) (*loggerLogrus, error) {
	var err error
	//once.Do(func() {
	//	loggerInstance, err = createLogger(config)
	//})

	loggerInstance, err = createLogger(config)
	return loggerInstance, err
}

// createLogger handles the actual logger creation
func createLogger(config *ConfigLogRus) (*loggerLogrus, error) {
	if config == nil {
		config = &ConfigLogRus{
			DefaultLevel: "trace",
			BasePrefix:   "service",
		}
	}

	if config.DefaultLevel == "" {
		config.DefaultLevel = "trace"
	}

	level, err := logrus.ParseLevel(config.DefaultLevel)
	if err != nil {
		level = logrus.TraceLevel
	}

	loggerNew := logrus.New()
	loggerNew.Out = os.Stderr
	loggerNew.Level = level
	//loggerNew.SetFormatter(&prefixed.TextFormatter{
	//	DisableColors:   false,
	//	TimestampFormat: "2006-01-02 15:04:05",
	//	FullTimestamp:   true,
	//	ForceFormatting: true,
	//})

	loggerNew.SetFormatter(&logrus2.Formatter{
		FieldsOrder:           []string{"component", "category", "req"},
		TimestampFormat:       "2006-01-02 15:04:05",
		HideKeys:              false,
		NoColors:              false,
		NoFieldsColors:        false,
		NoFieldsSpace:         false,
		ShowFullLevel:         true,
		NoUppercaseLevel:      false,
		TrimMessages:          false,
		CallerFirst:           true,
		CustomCallerFormatter: nil,
	})

	//loggerNew.SetReportCaller(true)

	if err := setupFileLogging(loggerNew); err != nil {
		return nil, err
	}

	telegramHook, err := setupTelegramHook(loggerNew)
	if err != nil {
		return nil, err
	}

	l1 := loggerLogrus{
		logger:       loggerNew,
		TelegramHook: telegramHook,
		cfg:          config,
		logLevel:     config.DefaultLevel,
	}
	return &l1, nil
}

// setupFileLogging configures file-based logging
func setupFileLogging(logger *logrus.Logger) error {
	logToFile := os.Getenv("LOG_FILE")
	logPath := os.Getenv("LOG_PATH")

	if logToFile != "true" {
		return nil
	}

	if logPath == "" {
		logPath = "./logs"
	}

	if err := makeDirectoryIfNotExists(logPath); err != nil {
		return fmt.Errorf("failed to create log directory: %w", err)
	}

	filePath := fmt.Sprintf("%s/%s_app.log", logPath, time.Now().Format("20060102"))
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("failed to open log file: %w", err)
	}

	logger.Out = io.MultiWriter(file, os.Stdout)
	logger.SetFormatter(&logrusFormatter{
		TextFormatter: logrus.TextFormatter{
			TimestampFormat:        "2006-01-02 15:04:05",
			DisableColors:          true,
			DisableLevelTruncation: true,
		},
	})
	return nil
}

// setupTelegramHook configures Telegram logging
func setupTelegramHook(logger *logrus.Logger) (*telegram_hook.TelegramHook, error) {
	telegramToken := os.Getenv("TELEGRAM_TOKEN")
	if telegramToken == "" {
		return nil, nil
	}

	name := os.Getenv("NAME")
	targetID := os.Getenv("TARGET_ID")
	threadID := cast.ToInt(os.Getenv("THREAD_ID"))

	hook, err := telegram_hook.NewTelegramHook(
		name,
		telegramToken,
		targetID,
		threadID,
		telegram_hook.WithAsync(true),
		telegram_hook.WithTimeout(30*time.Second),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create Telegram hook: %w", err)
	}

	logger.Hooks.Add(hook)
	return hook, nil
}

// makeDirectoryIfNotExists ensures the log directory exists
func makeDirectoryIfNotExists(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.MkdirAll(path, 0755); err != nil {
			return err
		}
	}
	return nil
}

// --------------------- loggerLogrus Methods ---------------------

func (al *loggerLogrus) GetLevel() string {
	return al.logger.Level.String()
}

func (al *loggerLogrus) Debug(args ...interface{}) {
	al.logger.Debug(args...)
}

func (al *loggerLogrus) Info(args ...interface{}) {
	al.logger.Info(args...)
}

func (al *loggerLogrus) Warn(args ...interface{}) {
	al.logger.Warn(args...)
}

func (al *loggerLogrus) Error(args ...interface{}) {
	entry := logrus.NewEntry(al.logger)
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()

	entry = entry.
		WithField("Path", fmt.Sprintf("%s:%v", frame.File, frame.Line)).
		WithField("Function", frame.Function)

	entry.Error(args...)
}

func (al *loggerLogrus) Fatal(args ...interface{}) {
	al.logger.Fatal(args...)
}

func (al *loggerLogrus) Panic(args ...interface{}) {
	al.logger.Panic(args...)
}

func (al *loggerLogrus) Print(args ...interface{}) {
	al.logger.Print(args...)
}

func (al *loggerLogrus) Debugln(i ...interface{}) {
	al.logger.Debugln(i...)
}

func (al *loggerLogrus) Debugf(s string, i ...interface{}) {
	al.logger.Debugf(s, i...)
}

func (al *loggerLogrus) Infoln(i ...interface{}) {
	al.logger.Infoln(i...)
}

func (al *loggerLogrus) Infof(s string, i ...interface{}) {
	al.logger.Infof(s, i...)
}

func (al *loggerLogrus) Warnln(i ...interface{}) {
	al.logger.Warnln(i...)
}

func (al *loggerLogrus) Warnf(s string, i ...interface{}) {
	al.logger.Warnf(s, i...)
}

func (al *loggerLogrus) Errorln(i ...interface{}) {
	al.logger.Errorln(i...)
}

func (al *loggerLogrus) Errorf(s string, i ...interface{}) {
	al.logger.Errorf(s, i...)
}

func (al *loggerLogrus) Fatalln(i ...interface{}) {
	al.logger.Fatalln(i...)
}

func (al *loggerLogrus) Fatalf(s string, i ...interface{}) {
	al.logger.Fatalf(s, i...)
}

func (al *loggerLogrus) Panicln(i ...interface{}) {
	al.logger.Panicln(i...)
}

func (al *loggerLogrus) Panicf(s string, i ...interface{}) {
	al.logger.Panicf(s, i...)
}

func (al *loggerLogrus) With(key string, value interface{}) LoggerInterface {
	return &loggerLogrus{
		logger: al.logger.WithField(key, value).Logger,
		cfg:    al.cfg,
	}
}

func (al *loggerLogrus) Withs(fields Fields) LoggerInterface {
	return &loggerLogrus{
		logger: al.logger.WithFields(logrus.Fields(fields)).Logger,
		cfg:    al.cfg,
	}
}

func (al *loggerLogrus) WithSrc() LoggerInterface {
	pc, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "unknown"
		line = 0
	}
	function := runtime.FuncForPC(pc).Name()
	return al.Withs(Fields{
		"source": fmt.Sprintf("%s:%d (%s)", file, line, function),
	})
}

func (al *loggerLogrus) WithError(err error) LoggerInterface {
	return &loggerLogrus{
		logger: al.logger.WithError(err).Logger,
		cfg:    al.cfg,
	}
}

func (al *loggerLogrus) WithField(key string, value interface{}) LoggerInterface {
	return &loggerLogrus{
		logger: al.logger.WithField(key, value).Logger,
		cfg:    al.cfg,
	}
}

func (al *loggerLogrus) WithFields(fields map[string]interface{}) LoggerInterface {
	return &loggerLogrus{
		logger: al.logger.WithFields(fields).Logger,
		cfg:    al.cfg,
	}
}

// --------------------- Telegram Notification ---------------------

func (al *loggerLogrus) SendErrorChatTelegram(err error, config ...telegram_hook.Config) {
	if len(os.Getenv("TELEGRAM_TOKEN")) == 0 {
		al.logger.Error("TELEGRAM_TOKEN not set")
		return
	}

	if err != nil {
		go al.TelegramHook.SendMessage(err.Error(), config...)
	}
}

func (al *loggerLogrus) SendMessageChatTelegram(message string, config ...telegram_hook.Config) {
	if len(os.Getenv("TELEGRAM_TOKEN")) == 0 {
		al.logger.Error("TELEGRAM_TOKEN not set")
		return
	}

	go al.TelegramHook.SendMessage(message, config...)
}
