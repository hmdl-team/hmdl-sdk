package sdk

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"github.com/golang-jwt/jwt/v4"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

func GetUid(usr interface{}) int {
	user := usr.(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	return int(claims["id"].(float64))
}

func MaHoa(data string) string {

	secret := "p@ssw0rdhmdl"

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	sha := hex.EncodeToString(h.Sum(nil))

	return sha
}

func Escape(source string) string {
	var j int = 0
	if len(source) == 0 {
		return ""
	}
	tempStr := source[:]

	desc := make([]byte, len(tempStr)*2)
	for i := 0; i < len(tempStr); i++ {
		flag := false
		var escape byte
		switch tempStr[i] {
		case '\r':
			flag = true
			escape = '\r'
			break
		case '\n':
			flag = true
			escape = '\n'
			break
		case '\\':
			flag = true
			escape = '\\'
			break
		case '\'':
			flag = true
			escape = '\''
			break
		case '"':
			flag = true
			escape = '"'
			break
		case '\032':
			flag = true
			escape = 'Z'
			break
		default:
		}
		if flag {
			desc[j] = '\\'
			desc[j+1] = escape
			j = j + 2
		} else {
			desc[j] = tempStr[i]
			j = j + 1
		}
	}
	return string(desc[0:j])

}

func TaoChuoi(dsId []int) string {

	inCondition := ""
	for _, item := range dsId {
		if inCondition != "" {
			inCondition += ", "
		}
		if item > 0 {
			inCondition += strconv.Itoa(item)
		}
	}

	return inCondition

}

// GetMonthFromTwoTime Get total number month in 2 time
func GetMonthFromTwoTime(a, b time.Time) float32 {

	var year, month, day, hour, min, sec int

	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = y2 - y1
	month = int(M2 - M1)
	day = d2 - d1
	hour = h2 - h1
	min = m2 - m1
	sec = s2 - s1

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return float32((year * 12) + month + (day / 31))
}

// Separate objects into several size
func splitObjects(objArr []interface{}, size int) [][]interface{} {
	var chunkSet [][]interface{}
	var chunk []interface{}

	for len(objArr) > size {
		chunk, objArr = objArr[:size], objArr[size:]
		chunkSet = append(chunkSet, chunk)
	}
	if len(objArr) > 0 {
		chunkSet = append(chunkSet, objArr[:])
	}

	return chunkSet
}

// Enable map keys to be retrieved in same order when iterating
func sortedKeys(val map[string]interface{}) []string {
	var keys []string
	for key := range val {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

// Check if string value is contained in slice
func containString(s []string, value string) bool {
	for _, v := range s {
		if v == value {
			return true
		}
	}
	return false
}

func docSoBaChuSo(baso int) string {

	ChuSo := []string{" không", " một", " hai", " ba", " bốn", " năm", " sáu", " bảy", " tám", " chín"}

	ketQua := ""

	tram := baso / 100
	chuc := (baso % 100) / 10
	donvi := baso % 10

	if tram == 0 && chuc == 0 && donvi == 0 {
		return ""
	}

	if tram != 0 {

		ketQua += ChuSo[tram] + " trăm "
		if chuc == 0 && donvi != 0 {
			ketQua += " linh"
		}

	}

	if chuc != 0 && chuc != 1 {
		ketQua += ChuSo[chuc] + " mươi"
		if chuc == 0 && donvi != 0 {
			ketQua = ketQua + " linh "

		}
	}

	if chuc == 1 {
		ketQua += " mười"
	}

	switch donvi {
	case 1:
		if chuc != 0 && chuc != 1 {
			ketQua += " mốt "
		} else {
			ketQua += ChuSo[donvi]
		}
		break
	case 5:
		if chuc == 0 {
			ketQua += ChuSo[donvi]
		} else {
			ketQua += " lăm "
		}
		break
	default:
		if donvi != 0 {
			ketQua += ChuSo[donvi]
		}
		break
	}
	return ketQua
}

func makeFirstUpCase(s string) string {

	if len(s) < 2 {
		return strings.ToUpper(s)
	}

	bts := []byte(s)

	lc := bytes.ToUpper([]byte{bts[0]})
	rest := bts[1:]

	return string(bytes.Join([][]byte{lc, rest}, nil))
}

func DocTienBangChu(SoTien float64) string {
	Tien := []string{"", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ"}

	lan := 0

	var so float64 = 0
	KetQua := ""
	tmp := ""

	if SoTien < 0 {
		return "Số tiền âm !"
	}

	if SoTien == 0 {
		return "Không đồng !"
	}

	if SoTien > 0 {
		so = SoTien
	} else {
		so = -SoTien
	}

	if SoTien > 8999999999999999 {
		//SoTien = 0;
		return "Số quá lớn!"
	}

	viTri := make(map[int]float64, 0)

	viTri[5] = math.Floor(so / 1000000000000000)
	so = so - viTri[5]*1000000000000000

	viTri[4] = math.Floor(so / 1000000000000)
	so = so - viTri[4]*1000000000000

	viTri[3] = math.Floor(so / 1000000000)
	so = so - viTri[3]*1000000000

	viTri[2] = math.Floor(so / 1000000)
	viTri[1] = math.Floor(float64((int(so) % 1000000) / 1000))
	viTri[0] = math.Floor(float64(int(so) % 1000))

	if viTri[5] > 0 {
		lan = 5
	} else if viTri[4] > 0 {
		lan = 4
	} else if viTri[3] > 0 {
		lan = 3
	} else if viTri[2] > 0 {
		lan = 2
	} else if viTri[1] > 0 {
		lan = 1
	} else {
		lan = 0
	}

	for i := lan; i >= 0; i-- {

		tmp = docSoBaChuSo(int(viTri[i]))
		KetQua += tmp
		if viTri[i] > 0 {
			KetQua += Tien[i]
		}
		if (i > 0) && (len(tmp) > 0) {
			KetQua += "," //&& (!string.IsNullOrEmpty(tmp))
		}
	}

	if KetQua[1:] == "," {
		KetQua = KetQua[0 : len(KetQua)-1]
	}

	KetQua = makeFirstUpCase(strings.TrimLeft(KetQua, " "))
	KetQua = makeFirstUpCase(strings.Trim(KetQua, " "))
	KetQua = strings.Replace(KetQua, "  ", " ", -2)

	return KetQua
}

func GetNumberInString(chuoi string) *int {
	re := regexp.MustCompile("[0-9]+")
	allString := re.FindAllString(chuoi, -1)
	if len(allString) > 0 {
		value, err := StringToInt(allString[0])
		if err != nil {
			return nil
		}
		return &value
	}

	return nil

}

// Acronym Acronym: từ viết tắt - lấy ký tự đầu mỗi từ trong chuỗi
func Acronym(s string) (acr string) {

	var a []string
	a = strings.Split(s, " ")
	for _, word := range a {
		acr = acr + string(word[0])
	}
	acr = strings.ToUpper(acr)
	return

}

func SplitString(text, separator string) []string {
	result := make([]string, 0)
	parts := strings.Split(text, separator)
	for _, p := range parts {
		result = append(result, strings.TrimSpace(p))
	}
	return result
}
