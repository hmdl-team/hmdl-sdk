package sdk

import (
	"fmt"
	"github.com/rotisserie/eris"
	"io/ioutil"
	"os"
	"time"
)

type LogConfig struct {
	LogFolder     string // thư mục chứa log file. Nếu rỗng có nghĩa là không ghi log ra file
	ErrorTemplate string // tên view template sẽ render error page
	InfoTemplate  string // tên view template sẽ render info page
	Top           int    // số dòng đỉnh stack trace sẽ được in ra
}

var LogConf *LogConfig
var logFile *os.File

var ErisStringFormat eris.StringFormat

// Init Dùng variadic param chỉ để tạo ra một optional param: có thể có hoặc không
//Nếu không truyền tham số thì sẽ tự tạo tham số mặc định
func Init(logConfig ...LogConfig) *os.File {
	if len(logConfig) > 0 {
		LogConf = &logConfig[0]
	} else { //Truyền cấu hình nil thì tạo cấu hình mặc định
		LogConf = &LogConfig{
			LogFolder:     "logs/", // thư mục chứa log file. Nếu rỗng có nghĩa là không ghi log ra file
			ErrorTemplate: "error", // tên view template sẽ render error page
			InfoTemplate:  "info",  // tên view template sẽ render info page
			Top:           3,       // số dòng đầu tiên trong stack trace sẽ được giữ lại
		}
	}

	ErisStringFormat = eris.StringFormat{
		Options: eris.FormatOptions{
			InvertOutput: false, // flag that inverts the error output (wrap errors shown first)
			WithTrace:    true,  // flag that enables stack trace output
			InvertTrace:  true,  // flag that inverts the stack trace output (top of call stack shown first)
			WithExternal: false,
			//Mục tiêu để báo lỗi gọn hơn, stack trace đủ ngắn
		},
		MsgStackSep:  "\n",  // separator between error messages and stack frame data
		PreStackSep:  "\t",  // separator at the beginning of each stack frame
		StackElemSep: " | ", // separator between elements of each stack frame
		ErrorSep:     "\n",  // separator between each error in the chain
	}

	if LogConf.LogFolder != "" {
		deleteZeroByteLogFiles(LogConf.LogFolder)
		logFile = newLogFile(LogConf.LogFolder)
		return logFile
	} else {
		return nil
	}
}

/*
Loại bỏ những log file cũ nhưng không có dữ liệu được ghi toàn là zero byte
*/
func deleteZeroByteLogFiles(logFolder string) {
	files, err := ioutil.ReadDir(logFolder)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, file := range files {
		if !file.IsDir() && file.Size() == 0 {
			os.Remove(logFolder + "/" + file.Name())
		}
	}
}
func todayFilename() string {
	today := time.Now().Format("2006 01 02")
	return today + ".txt"
}

func newLogFile(logFolder string) *os.File {
	if logFolder == "" {
		return nil
	}
	filename := todayFilename()
	// Open the file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(logFolder+filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}

	return f
}
