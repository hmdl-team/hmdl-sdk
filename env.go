package sdk

import (
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

var (
	envProduction  = []string{"production", "pro"}
	envDevelopment = []string{"development", "dev", "developer"}
)

func LoadEnv() {
	loadEnvFile(".env")
	env := strings.ToLower(getEnvWithDefault("ENVIRONMENT", envProduction[0]))
	base := os.Getenv("BASE")

	files := []string{
		filepath.Join(base, ".env"),
		".env",
	}

	if contains(envProduction, env) {
		files = append(files, filepath.Join(base, "pro.env"), "pro.env")
	} else if contains(envDevelopment, env) {
		files = append(files, filepath.Join(base, "dev.env"), "dev.env")
	} else {
		logrus.Warnf("Unknown environment: %s", env)
	}

	for _, file := range files {
		loadEnvFile(file)
	}
}

func IsProduction() bool {
	return contains(envProduction, os.Getenv("ENVIRONMENT"))
}

func IsDevelopment() bool {
	return contains(envDevelopment, os.Getenv("ENVIRONMENT"))
}

func IsDebugLog() bool {
	return os.Getenv("DEBUG_LOG") == "true"
}

func IsDebugMain() bool {
	return len(os.Getenv("LOG_EMAIL")) > 0
}

func getEnvWithDefault(key, defaultValue string) string {
	if value := os.Getenv(key); value != "" {
		return value
	}
	return defaultValue
}

func loadEnvFile(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		logrus.Debugf("Env file does not exist: %s", path)
		return
	}
	if err := godotenv.Load(path); err != nil {
		logrus.Warnf("Failed to load env file: %s, error: %v", path, err)
	}
}

func GetEnvironment() string {
	return getEnvWithDefault("ENVIRONMENT", envProduction[0])
}

func contains(arr []string, str string) bool {
	for _, v := range arr {
		if v == str {
			return true
		}
	}
	return false
}
