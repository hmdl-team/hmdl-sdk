package sdk

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
)

type Response struct {
	StatusCode int         `json:"code"`
	Success    bool        `json:"success"`
	Message    string      `json:"message,omitempty"`
	Paging     interface{} `json:"paging,omitempty"`
	Data       interface{} `json:"data,omitempty"`
	Input      interface{} `json:"input,omitempty"`
}

func (s *Response) Error() string {
	return fmt.Sprint(s.Message)
}

func (s *Response) WithMessage(message string) *Response {
	s.Message = message
	return s
}

func (s *Response) WithInput(input interface{}) *Response {
	s.Input = input
	return s
}

func (s *Response) WithPaging(paging interface{}) *Response {
	s.Paging = paging
	return s
}

func (s *Response) WithData(data interface{}) *Response {
	s.Data = data
	return s
}

func (s *Response) WithSuccess(success bool) *Response {
	s.Success = success
	return s
}

func (s *Response) WithStatusCode(statusCode int) *Response {
	s.StatusCode = statusCode
	return s
}

func (s *Response) ToBytes() []byte {
	data, _ := json.Marshal(*s)
	return data
}

func NewSuccessResponse(data interface{}) *Response {
	return &Response{
		StatusCode: http.StatusOK,
		Success:    true,
		Data:       data,
	}
}

func NewSuccessWithMessageResponse(message string) *Response {
	return &Response{
		StatusCode: http.StatusOK,
		Message:    message,
		Success:    true,
	}
}

func NewSuccessWithMessageResponseAndData(data interface{}, message string) *Response {
	return &Response{
		StatusCode: http.StatusOK,
		Message:    message,
		Data:       data,
		Success:    true,
	}
}

func ResponseWithCode(c echo.Context, code int, errMsg ...string) error {
	var msg string
	if len(errMsg) == 0 {
		msg = http.StatusText(code)
	} else {
		msg = errMsg[0]
	}
	return c.JSON(code, Response{
		StatusCode: code,
		Message:    msg,
		Success:    true,
	})
}

func ResponseDataMessage(c echo.Context, mesage string, data interface{}) error {
	return c.JSON(http.StatusOK, Response{
		StatusCode: http.StatusOK,
		Message:    mesage,
		Data:       data,
		Success:    true,
	})
}

func ResponseData(c echo.Context, data interface{}) error {
	return c.JSON(http.StatusOK, Response{
		StatusCode: http.StatusOK,
		Message:    http.StatusText(http.StatusOK),
		Data:       data,
		Success:    true,
	})
}
