package echoc

import (
	"flag"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	echocMid "gitlab.com/hmdl-team/hmdl-sdk/component/echoc/middleware"
	"net/http"
	"os"
	"time"
)

const (
	defaultPort = 3000
	defaultMode = "debug"
)

type Config struct {
	port     int
	echoMode string
}

type echoEngine struct {
	*Config
	name   string
	id     string
	logger sdk.LoggerInterface
	router *echo.Echo
}

func (gs *echoEngine) Start() error {

	return nil
}

func NewEcho(id string) sdk.ECHOComponent {
	return &echoEngine{
		Config: new(Config),
		id:     id,
	}
}

func (gs *echoEngine) ID() string {
	return gs.id
}

func (gs *echoEngine) Activate(sv sdk.ServiceContext) error {
	gs.logger = sv.Logger(gs.id)
	gs.name = sv.GetName()

	gs.logger.Info("init engine...")
	e := echo.New()

	e.Use(echocMid.CustomContextMiddleware)
	e.Use(echocMid.CustomCheckErrorMiddleware)

	//e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	AllowCrossOrigin := os.Getenv("CORS_ORIGINS_CUSTOM") == "true"
	if !AllowCrossOrigin {
		e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: []string{"https://*", "http://*"},
			AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
			//AllowHeaders:     []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
			AllowHeaders:     []string{"*"},
			AllowCredentials: true,
			MaxAge:           300,
		}))
	}

	e.GET("/ping", func(c echo.Context) error {
		return c.JSON(200, map[string]interface{}{
			"code":    200,
			"message": "pong",
		})
	})

	gs.router = e

	return nil
}

func (gs *echoEngine) Stop() error {
	return nil
}

func (gs *echoEngine) InitFlags() {
	flag.IntVar(&gs.Config.port, "PORT", defaultPort, "echo server port. Default 3000")
}

func (gs *echoEngine) GetPort() int {
	return gs.port
}

func (gs *echoEngine) GetRouter() *echo.Echo {
	return gs.router
}

func (gs *echoEngine) GetHttpServer() *http.Server {
	server := &http.Server{
		Addr:         fmt.Sprintf(":%v", gs.GetPort()),
		Handler:      gs.router,
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}
	return server
}
