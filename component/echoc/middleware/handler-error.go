package middleware

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/color"
	"github.com/valyala/fasttemplate"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	"io"
	"os"
	"runtime/debug"
	"sync"
	"time"
)

func CustomCheckErrorMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Thay đổi Context thành HandlerContext
		cc := &sdk.HandlerContext{
			Context: c,
		}

		err := next(cc)
		if err != nil {
			// handle error
			if errA, ok := err.(*sdk.AppError); ok {

				// only log from 500 error
				if errA.StatusCode >= 500 {
					errRes := errA.WithApiPath(cc.Path())
					info, _ := debug.ReadBuildInfo()
					errRes.Module = info.Main.Path

					return cc.JSON(errA.StatusCode, errRes)
				}
				return cc.JSON(errA.StatusCode, errA.CheckErrorProduction())
			}

			if err, ok := err.(*echo.HTTPError); ok {
				return err
			}

			if res, ok := err.(*sdk.Response); ok {
				return cc.JSON(res.StatusCode, res)
			}

			//	s.Logger().Errorln(err)
			return cc.InternalServerError(err)
		}

		return nil
	}
}

func LoggerMiddleware() echo.MiddlewareFunc {
	format := `👉 [${method}] ${icon} [${status}] [${path}] [${time_rfc3339}] [${remote_ip}] [${host}] ${error}` + "\n"
	pool := &sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 256))
		},
	}
	template := fasttemplate.New(format, "${", "}")
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			req := c.Request()
			res := c.Response()
			// start := time.Now()
			if err = next(c); err != nil {
				c.Error(err)
			}
			// stop := time.Now()
			buf := pool.Get().(*bytes.Buffer)
			buf.Reset()
			defer pool.Put(buf)
			colored := color.New()
			_, err = template.ExecuteFunc(buf, func(w io.Writer, tag string) (int, error) {
				switch tag {
				case "status":
					n := res.Status
					s := colored.Green(n)
					switch {
					case n >= 500:
						s = colored.Red(n)
					case n >= 400:
						s = colored.Yellow(n)
					case n >= 300:
						s = colored.Cyan(n)
					}
					return buf.WriteString(s)
				case "icon":
					n := res.Status
					s := `✅`
					switch {
					case n >= 500:
						s = `💥`
					case n >= 400:
						s = `❗`
					}
					return buf.WriteString(s)
				case "uri":
					return buf.WriteString(req.RequestURI)
				case "method":
					n := req.Method
					s := colored.Green(n)
					switch n {
					case echo.POST:
						s = colored.Yellow(n)
					case echo.PUT:
						s = colored.Blue(n)
					case echo.PATCH:
						s = colored.Cyan(n)
					case echo.DELETE:
						s = colored.Red(n)
					}
					return buf.WriteString(s)
				case "path":
					p := req.URL.Path
					if p == "" {
						p = "/"
					}
					return buf.WriteString(p)
				case "host":
					p := req.URL.Host
					return buf.WriteString(p)
				case "remote_ip":
					p := req.RemoteAddr
					return buf.WriteString(p)
				case "error":
					if err != nil {
						// Error may contain invalid JSON e.g. `"`
						b, _ := json.Marshal(fmt.Sprintf(`[%s]`, err.Error()))
						b = b[1 : len(b)-1]
						return buf.Write(b)
					}
				case "time_rfc3339":
					return buf.WriteString(time.Now().Format(time.RFC3339))
				}

				return 0, nil
			})

			if err != nil {
				return
			}

			// middleware.DefaultLoggerConfig.Output
			//if config.Output == nil {
			//	_, err = c.LoggerInterface().Output().Write(buf.Bytes())
			//	return
			//}

			_, err = os.Stdout.Write(buf.Bytes())

			return
		}
	}
}
