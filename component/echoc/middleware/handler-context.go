package middleware

import (
	"github.com/labstack/echo/v4"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
)

// Middleware thay đổi Context mặc định thành HandlerContext
func CustomContextMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Thay đổi Context thành HandlerContext
		cc := &sdk.HandlerContext{
			Context: c,
		}

		return next(cc)
	}
}
