package natsc

import (
	"flag"
	"fmt"
	"github.com/nats-io/nats.go"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	"os"
	"time"
)

const (
	defaultRetryCount = 3
	defaultRetryDelay = 2 * time.Second
	defaultNatUriEnv  = "NAT_URI"
)

var (
	Nat     *nats.Conn
	NatJson *nats.EncodedConn
)

// Config giữ cấu hình NATS
type Config struct {
	port int
	url  string
}

// natsEngine đại diện cho lớp kết nối NATS
type natsEngine struct {
	*Config
	id     string
	logger sdk.LoggerInterface
}

// NewNatsc Tạo một engine NATS mới
func NewNatsc(id string) *natsEngine {
	return &natsEngine{
		Config: new(Config),
		id:     id,
	}
}

// ID Lấy ID của engine
func (gs *natsEngine) ID() string {
	return gs.id
}

// Activate Kích hoạt và kết nối NATS
func (gs *natsEngine) Activate(sv sdk.ServiceContext) error {
	gs.logger = sv.Logger(gs.id)
	gs.url = os.Getenv(defaultNatUriEnv)
	if gs.url == "" {
		gs.logger.Warnln("🚫 NAT_URI không được cấu hình. Bỏ qua kết nối NAT.")
		return nil
	}

	gs.logger.Infof("👉 Đang kết nối NATS tại: %s", gs.url)
	nc, err := connectWithRetry(gs.url, defaultRetryCount, defaultRetryDelay, gs.logger)
	if err != nil {
		return err
	}

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		nc.Close()
		return fmt.Errorf("lỗi tạo encoded connection: %w", err)
	}

	Nat = nc
	NatJson = ec
	gs.logger.Infof("👍 Kết nối thành công NATS tại: %s", gs.url)
	return nil
}

// Stop Ngắt kết nối NATS
func (gs *natsEngine) Stop() error {
	if Nat != nil {
		Nat.Close()
		gs.logger.Info("🔌 Đã ngắt kết nối NATS.")
	}
	return nil
}

// InitFlags Khởi tạo cờ lệnh
func (gs *natsEngine) InitFlags() {
	flag.StringVar(&gs.Config.url, "NAT_URI", defaultNatUriEnv, "NAT_URI server port (mặc định nats://localhost:4222)")
}

// GetURL Lấy cổng NATS
func (gs *natsEngine) GetURL() string {
	return gs.url
}

// GetNats Lấy kết nối NATS
func (gs *natsEngine) GetNats() *nats.Conn {
	return Nat
}

// GetNatJson Lấy kết nối NATS JSON
func (gs *natsEngine) GetNatJson() *nats.EncodedConn {
	return NatJson
}

// connectWithRetry Thử lại kết nối NATS
func connectWithRetry(url string, retryCount int, retryDelay time.Duration, logger sdk.LoggerInterface) (*nats.Conn, error) {
	var nc *nats.Conn
	var err error
	for i := 1; i <= retryCount; i++ {
		nc, err = nats.Connect(url)
		if err == nil {
			return nc, nil
		}
		logger.Warnf("❌ Lỗi kết nối NATS (lần %d/%d): %v", i, retryCount, err)
		time.Sleep(retryDelay)
	}
	return nil, fmt.Errorf("không thể kết nối NATS sau %d lần thử: %w", retryCount, err)
}
