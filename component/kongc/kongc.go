package kongc

import (
	"github.com/congnguyendl/gokong/v6"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	sdk "gitlab.com/hmdl-team/hmdl-sdk"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

const (
	defaultPort = 8000
)

type Config struct {
	port           int
	echoMode       string
	IpService      string
	ServerAddress  string
	ServiceName    string
	ServicePath    string
	ServicePort    string
	StripPath      string
	PreserveHost   string
	Retries        string
	ConnectTimeout string
}

func externalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		address, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range address {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}

type kongEngine struct {
	*Config
	name   string
	id     string
	logger sdk.LoggerInterface
}

func NewKong(id string) *kongEngine {
	return &kongEngine{
		Config: new(Config),
		id:     id,
	}
}

func (u *kongEngine) ID() string {
	return u.id
}

func (u *kongEngine) Activate(sctx sdk.ServiceContext) error {
	u.logger = sctx.Logger(u.id)
	hostIp := sdk.GetLocalIP().String()

	ipLocal, err := externalIP()
	if err != nil {
		logrus.Warning(err)
	}

	u.logger.Infof("HOST IP: %s", hostIp)
	u.logger.Infof("CONTAINER IP: %s", ipLocal)

	if hostIp == "" {
		// GET IP của docker (Virtual bridge docker0)
		hostIp = ipLocal
	}

	u.ServerAddress = os.Getenv("KONG_ADDRESS")
	u.ServiceName = os.Getenv("KONG_SERVICE_NAME")
	u.ServicePort = os.Getenv("PORT")
	u.ServicePath = os.Getenv("KONG_SERVICE_PATH")
	u.StripPath = os.Getenv("KONG_STRIP_PATH")
	u.PreserveHost = os.Getenv("KONG_PRESERVE_PATH")
	u.Retries = os.Getenv("KONG_RETRIES")
	u.ConnectTimeout = os.Getenv("KONG_CONNECT_TIMEOUT")
	u.IpService = hostIp

	// Đăng ký service vs Kong Gate API
	ticker := time.NewTicker(2 * time.Second)
	go func(ticker *time.Ticker) {
		for {
			select {
			case <-ticker.C:
				if err := u.RegisterKong(); err == nil {
				} else {
					log.Println(err)
				}
				ticker.Stop()

			}
		}
	}(ticker)

	return nil
}

func (u *kongEngine) Check() error {

	if len(u.ServerAddress) == 0 {
		return errors.New("KONG_ADDRESS chưa được khai báo, module Kong sẽ không được sử dụng.")
	}
	if len(u.ServiceName) == 0 {
		return errors.New("KONG_SERVICE_NAME chưa được khai báo, module Kong sẽ không được sử dụng.")
	}
	if len(u.ServicePort) == 0 {
		return errors.New("PORT chưa được khai báo, module Kong sẽ không được sử dụng.")
	}
	if len(u.ServicePath) == 0 {
		return errors.New("KONG_SERVICE_PATH chưa được khai báo, module Kong sẽ không được sử dụng.")
	}
	if len(u.StripPath) == 0 {

	}

	if len(u.Retries) == 0 {
		u.Retries = "0"
	}
	if len(u.ConnectTimeout) == 0 {
		u.ConnectTimeout = "180000" //3 Minute
	}
	return nil
}

func (u *kongEngine) RegisterKong() error {

	if err := u.Check(); err != nil {
		return err
	}

	config := gokong.Config{HostAddress: u.ServerAddress}

	client := gokong.NewClient(&config)

	nameStream := strings.ToLower(u.ServiceName + "-upstream")
	// Tìm upstream
	upstream, err := client.Upstreams().GetByName(nameStream)

	//Nêu chưa có tạo mới upstream
	if upstream == nil {
		upstreamRequest := &gokong.UpstreamRequest{
			Name:  nameStream,
			Slots: 1000,
		}

		upstream, err = client.Upstreams().Create(upstreamRequest)

		if err != nil {
			return err
		}

	}

	if upstream != nil {

		targetOld, err := client.Targets().GetTargetsFromUpstreamId(upstream.Id)

		if err != nil {
			return err
		}

		for _, tarOld := range targetOld {
			err = client.Targets().DeleteFromUpstreamById(upstream.Id, *tarOld.Id)
			if err != nil {
				return err
			}
		}

		targetRequest := &gokong.TargetRequest{
			Target: u.IpService + ":" + u.ServicePort,
			Weight: 100,
		}
		_, err = client.Targets().CreateFromUpstreamId(upstream.Id, targetRequest)

		if err != nil {
			return err
		}

		timeOut := cast.ToInt(u.ConnectTimeout)
		// Đăng ký service
		serviceRequest := &gokong.ServiceRequest{
			Name:           gokong.String(strings.ToLower(u.ServiceName)),
			Protocol:       gokong.String("http"),
			Host:           gokong.String(strings.ToLower(upstream.Name)),
			Port:           nil,
			Path:           nil,
			Retries:        gokong.Int(cast.ToInt(u.Retries)),
			ConnectTimeout: &timeOut,
			WriteTimeout:   &timeOut,
			ReadTimeout:    &timeOut,
			Url:            nil,
			Tags:           nil,
		}

		// Kiểm tra đã có service chưa
		createdService, err := client.Services().GetServiceByName(*serviceRequest.Name)

		if err != nil {
			return err
		}

		// Nếu chưa thì tạo service
		if createdService == nil {
			createdService, err = client.Services().Create(serviceRequest)
			if err != nil {
				return err
			}
		}

		// Nếu đã tồn tại thì update
		if createdService != nil && createdService.Id != nil {
			createdService, err = client.Services().UpdateServiceById(*createdService.Id, serviceRequest)
			if err != nil {
				return err
			}
		}
		pathList := sdk.SplitString(u.ServicePath, ",")

		stripPath := true
		preserveHost := true

		if len(u.StripPath) > 0 && u.StripPath == "false" {
			stripPath = false
		}

		if len(u.PreserveHost) > 0 && u.PreserveHost == "false" {
			preserveHost = false
		}

		//Paths:         gokong.StringSlice([]string{strings.ToLower(u.ServicePath)}),
		if createdService != nil {
			routeRequest := &gokong.RouteRequest{
				Name:          gokong.String(strings.ToLower(*createdService.Name + "-ROUTER")),
				Protocols:     gokong.StringSlice([]string{"http", "https"}),
				Methods:       gokong.StringSlice([]string{"POST", "GET", "PUT", "DELETE", "OPTIONS", "HEAD", "CONNECT", "TRACE"}),
				Hosts:         nil,
				Paths:         gokong.StringSlice(pathList),
				RegexPriority: gokong.Int(0),
				StripPath:     gokong.Bool(stripPath),
				PreserveHost:  gokong.Bool(preserveHost),
				Snis:          nil,
				Sources:       nil,
				Destinations:  nil,
				Service:       gokong.ToId(*createdService.Id),
				Tags:          nil,
			}

			createdRoute, err := client.Routes().GetByName(*routeRequest.Name)

			if err != nil {
				return err
			}

			// Nếu chưa thì tạo router
			if createdRoute == nil {
				_, err := client.Routes().Create(routeRequest)
				if err != nil {
					return err
				}
			} else {
				_, err = client.Routes().UpdateById(*createdRoute.Id, routeRequest)
				if err != nil {
					return err
				}
			}
		}
	}

	u.logger.Infof("👍 Register Kong Sussess: %s:%s", u.IpService, u.ServicePort)
	return nil
}

func (u *kongEngine) Stop() error {
	return nil
}

func (u *kongEngine) InitFlags() {
	//	flag.IntVar(&gs.Config.port, "PORT", defaultPort, "kong server port. Default 8000")
}

func (u *kongEngine) GetPort() int {
	return u.port
}
