package sdk

import (
	"errors"
	"net/http"
)

var ErrNotFound = AppError{
	Status:  http.StatusText(http.StatusNotFound),
	Message: "The requested resource could not be found",
	Code:    http.StatusNotFound,
	Type:    WARNING,
	Key:     "NOT_FOUND",
}

var ErrUnauthorized = AppError{
	Status:  http.StatusText(http.StatusUnauthorized),
	Message: "The request could not be authorized",
	Code:    http.StatusUnauthorized,
	Type:    WARNING,
	Key:     "BAD_REQUEST",
}

var ErrForbidden = AppError{
	Status:  http.StatusText(http.StatusForbidden),
	Message: "The requested action was forbidden",
	Code:    http.StatusForbidden,
	Key:     "FORBIDDEN",
}

var ErrInternalServerError = AppError{
	Status:  http.StatusText(http.StatusInternalServerError),
	Message: "An internal server error occurred, please contact the system administrator",
	Code:    http.StatusInternalServerError,
	Type:    ERROR,
	Key:     "INTERNAL_SERVER_ERROR",
}

var ErrBadRequest = AppError{
	Status:  http.StatusText(http.StatusBadRequest),
	Message: "The request was malformed or contained invalid parameters",
	Code:    http.StatusBadRequest,
	Type:    WARNING,
	Key:     "BAD_REQUEST",
}

var ErrUnsupportedMediaType = AppError{
	Status:  http.StatusText(http.StatusUnsupportedMediaType),
	Message: "The request is using an unknown content type",
	Code:    http.StatusUnsupportedMediaType,
	Type:    WARNING,
	Key:     "UNSUPPORTED_MEDIA_TYPE",
}

var ErrConflict = AppError{
	Status:  http.StatusText(http.StatusConflict),
	Message: "The resource could not be created due to a conflict",
	Code:    http.StatusConflict,
	Type:    WARNING,
	Key:     "CONFLICT",
}

// ErrRecordNotFound is used to make our application logic independent of other libraries errors
var ErrRecordNotFound = errors.New("record not found")
