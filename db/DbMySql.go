package db

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"os"
)

type MySql struct {
	Host     string
	Port     string
	UserName string
	Password string
	DbName   string
}

func (u *MySql) Connect() (*gorm.DB, error) {

	config := gorm.Config{}

	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:               fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Asia%%2fSaigon", u.UserName, u.Password, u.Host, u.Port, u.DbName),
		DefaultStringSize: 256, // default size for string fields
	}), &config)

	if err != nil {
		return nil, err
	}

	debug := os.Getenv("DEBUG")

	if debug == "true" {
		db.Logger = db.Logger.LogMode(logger.Info)
	} else if debug == "false" {
		db.Logger = db.Logger.LogMode(logger.Error)
	}

	logrus.Infof("👍 Connected Database: %s", u.DbName)
	logrus.Infof("👍 DEBUG: %s", debug)

	return db, nil
}
