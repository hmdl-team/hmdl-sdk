package db

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

type MsSql struct {
	Host     string
	Port     string
	UserName string
	Password string
	DbName   string
}

//sql server
func (u *MsSql) Connect() (*gorm.DB, error) {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,
			LogLevel:      logger.Info,
			Colorful:      true,
		},
	)

	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%v;database=%s", u.Host, u.UserName, u.Password, u.Port, u.DbName)
	db, err := gorm.Open(sqlserver.Open(connectionString), &gorm.Config{
		Logger: newLogger,
	})

	if err != nil {
		return nil, err
	}

	debug := os.Getenv("DEBUG")

	if debug == "true" {
		db.Logger = db.Logger.LogMode(logger.Info)
	} else if debug == "false" {
		db.Logger = db.Logger.LogMode(logger.Error)
	}

	logrus.Infof("👍 Connected Database: %s", u.DbName)
	logrus.Infof("👍 DEBUG: %s", debug)

	return db, nil
}
