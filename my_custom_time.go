package sdk

import (
	_const "gitlab.com/hmdl-team/hmdl-sdk/const"
	"gitlab.com/hmdl-team/hmdl-sdk/helper"
	"time"
)

import (
	"encoding/xml"
	"strings"
)

type CustomTime time.Time

func (ct *CustomTime) IsNil() bool {
	return time.Time(*ct).IsZero()
}

func (ct *CustomTime) String() string {
	return time.Time(*ct).String()
}

func (ct *CustomTime) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var s string
	err := d.DecodeElement(&s, &start)
	if err != nil {
		return err
	}

	return ct.UnmarshalJSON([]byte(s))
}

func (ct *CustomTime) UnmarshalParam(param string) error {
	return ct.UnmarshalJSON([]byte(param))
}

func (ct *CustomTime) UnmarshalJSON(b []byte) error {

	if string(b) == "null" {
		return nil
	}

	s := strings.Trim(string(b), "\"")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "Z", "")
	s = strings.ReplaceAll(s, "z", "")

	dateTime, errDateIso := helper.ConvertStringToTime(s)

	if errDateIso != nil {
		return errDateIso
	}

	if dateTime != nil {
		*ct = CustomTime(*dateTime)
	}
	return nil
}

func (ct *CustomTime) ToTime() *time.Time {
	timeConvert := time.Time(*ct)
	if timeConvert.IsZero() {
		return nil
	}
	return &timeConvert
}

func (ct *CustomTime) ToSqlDateString() string {
	if ct == nil || ct.IsNil() {
		return ""
	}
	t := time.Time(*ct)
	return t.Format(_const.LayoutSQl)
}
