package sdk

type IUser interface {
	GetUid() string
	GetUserId() int
	GetRole() string
	GetDisplayName() string
	GetDuAnId() int
}
