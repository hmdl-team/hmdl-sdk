package sdk

import (
	"fmt"
	"github.com/jordan-wright/email"
	"net/smtp"
	"os"
)

type emailConfig struct {
	host       string
	port       string
	name       string
	pass       string
	adminEmail string
}

func GetEmailConfig() *emailConfig {
	config := emailConfig{
		host: os.Getenv("EMAIL_SERVER_HOST"),
		port: os.Getenv("EMAIL_SERVER_PORT"),
		name: os.Getenv("EMAIL_SERVER_NAME"),
		pass: os.Getenv("EMAIL_SERVER_PASS"),
	}

	return &config
}

func (config emailConfig) CheckConfigEmail() bool {
	if len(config.host) == 0 || len(config.port) == 0 || len(config.name) == 0 || len(config.pass) == 0 {
		return false
	}
	return true
}

type Email struct {
	To      []string
	Cc      []string
	From    string
	Subject string
	Text    string
	HTML    string
}

func NewEmail(to []string, subject string, text string) *Email {
	email := Email{To: to, Subject: subject, Text: text}

	return &email
}

func (emailData *Email) WithTo(to []string) *Email {
	emailData.To = to
	return emailData
}
func (emailData *Email) WithCc(Cc []string) *Email {
	emailData.Cc = Cc
	return emailData
}
func (emailData *Email) WithFrom(From string) *Email {
	emailData.From = From
	return emailData
}
func (emailData *Email) WithSubject(Subject string) *Email {
	emailData.Subject = Subject
	return emailData
}
func (emailData *Email) WithText(Text string) *Email {
	emailData.Text = Text
	return emailData
}
func (emailData *Email) WithHtml(HTML string) *Email {
	emailData.HTML = HTML
	return emailData
}
func (emailData *Email) SendEmail() error {
	configEmail := GetEmailConfig()
	if !configEmail.CheckConfigEmail() {
		return NewError("Config không tồn tại")
	}

	if len(emailData.From) == 0 {
		emailData.From = configEmail.name
	}

	e := email.NewEmail()
	e.From = emailData.From
	e.To = emailData.To
	e.Cc = emailData.Cc
	e.Subject = emailData.Subject
	e.Text = []byte(emailData.Text)
	if len(emailData.HTML) > 0 {
		e.HTML = []byte(emailData.HTML)
	}

	return e.Send(
		fmt.Sprintf(`%s:%s`, configEmail.host, configEmail.port),
		smtp.PlainAuth("", configEmail.name, configEmail.pass, configEmail.host),
	)
}
