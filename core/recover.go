package core

import sctx "gitlab.com/hmdl-team/hmdl-sdk"

func Recover() {
	if r := recover(); r != nil {
		sctx.GlobalLogger().GetLogger("recovered").Errorln(r)
	}
}
