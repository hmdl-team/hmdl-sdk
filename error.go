package sdk

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"net/http"
	"runtime"
)

// ErrorType defines levels of errors
// WARNING: Application can still run
// ERROR: Log error and notify the end-user
// SYSERROR: Notify Internal Server Error and log it
// PANIC: Critical error, log and exit application

type ErrorType int

const (
	WARNING ErrorType = iota + 1
	ERROR
	SYSERROR
	PANIC
)

// String returns the string representation of the ErrorType
func (e ErrorType) String() string {
	switch e {
	case WARNING:
		return "WARNING"
	case ERROR:
		return "ERROR"
	case SYSERROR:
		return "SYSERROR"
	case PANIC:
		return "PANIC"
	default:
		return "UNKNOWN"
	}
}

type AppError struct {
	StatusField string                 `json:"status_field,omitempty"`
	ID          string                 `json:"id,omitempty"`
	RequestID   string                 `json:"request,omitempty"`
	Status      string                 `json:"status,omitempty"`
	Code        int                    `json:"code"`
	Message     string                 `json:"message"`
	Module      string                 `json:"module,omitempty"`
	Log         string                 `json:"log,omitempty"`
	Key         string                 `json:"error_key,omitempty"`
	Input       interface{}            `json:"input,omitempty"`
	Type        ErrorType              `json:"err_type,omitempty"`
	Data        map[string]interface{} `json:"data,omitempty"`
	ErrorApp    map[string]interface{} `json:"error_app,omitempty"`
	RootError   error                  `json:"-"`
	StatusCode  int                    `json:"-"`
	Details     map[string]interface{} `json:"details,omitempty"`
	stackTrace  *stack
}

type AppErrorOption func(*AppError)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

// NewAppError creates a new AppError with optional configurations
func NewAppError(err error, opts ...AppErrorOption) *AppError {
	if err == nil {
		err = errors.New("Bad request")
	}

	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()

	e := &AppError{
		Key:        "BAD_REQUEST",
		StatusCode: http.StatusBadRequest,
		Code:       -1,
		RootError:  err,
		Message:    err.Error(),
		Log:        err.Error(),
		Type:       ERROR,
		stackTrace: callers(3),
		ErrorApp: map[string]interface{}{
			"File":     fmt.Sprintf("%s:%d", frame.File, frame.Line),
			"Function": frame.Function,
		},
	}

	for _, opt := range opts {
		opt(e)
	}

	return e
}

func NewErrorResponse(root error, msg, log, key string) *AppError {
	err := NewAppError(root)
	err.Message = msg
	err.Log = log
	err.Key = key
	return err
}

func NewCustomError(root error, msg, log, key string) *AppError {
	err := NewAppError(root)
	err.Message = msg
	err.Log = log
	err.Key = key
	return err
}

func NewFullErrorResponse(statusCode int, root error, msg, log, key string, ErrType ErrorType) *AppError {
	err := NewAppError(root)

	err.StatusCode = statusCode
	err.Message = msg
	err.Log = log
	err.Key = key
	err.Type = ErrType

	return err
}

// WithMessage sets a custom message for the AppError
func WithMessage(msg string) AppErrorOption {
	return func(e *AppError) {
		e.Message = msg
	}
}

// WithType sets the error type
func WithType(errType ErrorType) AppErrorOption {
	return func(e *AppError) {
		e.Type = errType
	}
}

// WithStatusCode sets the HTTP status code
func WithStatusCode(status int) AppErrorOption {
	return func(e *AppError) {
		e.StatusCode = status
		e.Status = http.StatusText(status)
	}
}

// WithData adds extra data to the error
func WithData(data map[string]interface{}) AppErrorOption {
	return func(e *AppError) {
		e.Data = data
	}
}

// StackTrace returns the stack trace of the error
func (err *AppError) StackTrace() errors.StackTrace {
	if err.RootError == nil {
		return nil
	}

	if tracer, ok := err.RootError.(stackTracer); ok {
		return tracer.StackTrace()
	}

	return nil
}

// CheckProduction sanitizes error data for production
func (err *AppError) CheckProduction() *AppError {
	if IsProduction() {
		err.Data = nil
		err.Log = ""
	}
	return err
}

// Error implements the error interface
func (err *AppError) Error() string {
	return err.Message
}

// ToJSON converts the AppError to JSON format
func (err *AppError) ToJSON() []byte {
	data, _ := json.Marshal(err)
	return data
}

func (err *AppError) ToBytes() []byte {
	data, _ := json.Marshal(*err)
	return data
}

// Helper functions to create specific errors
func NewBadRequestError(msg string) *AppError {
	return NewAppError(nil, WithMessage(msg), WithStatusCode(http.StatusBadRequest), WithType(WARNING))
}

func NewInternalError(err error) *AppError {
	return NewAppError(err, WithStatusCode(http.StatusInternalServerError), WithType(SYSERROR))
}

func NewCustomAppError(code int, message string) *AppError {
	return NewAppError(nil).
		WithMessage(message).
		WithCode(code).
		WithStatusCode(http.StatusOK).
		CheckErrorProduction()
}

func NewBadRequestErr(err error) *AppError {
	return NewAppError(err, WithStatusCode(http.StatusBadRequest), WithType(WARNING))
}

func NewNotFoundErr(err error) *AppError {
	return NewAppError(err, WithStatusCode(http.StatusNotFound), WithType(WARNING))
}

func NewUnauthorizedErr(err error) *AppError {
	return NewAppError(err, WithStatusCode(http.StatusNotFound), WithType(WARNING))
}

func NewUnauthorizedError(msg string) *AppError {
	return NewAppError(nil, WithMessage(msg), WithStatusCode(http.StatusUnauthorized), WithType(ERROR))
}

func NewNotFoundError(msg string) *AppError {
	return NewAppError(nil, WithMessage(msg), WithStatusCode(http.StatusNotFound), WithType(WARNING))
}
func NewError(msg string) *AppError {
	return NewAppError(nil, WithMessage(msg), WithStatusCode(http.StatusBadRequest), WithType(WARNING))
}

// callers generates stack trace (dummy implementation)
func callers(skip int) *stack {
	return &stack{}
}

type stack struct{}

func (s *stack) get() []runtime.Frame {
	// Placeholder for actual stack trace retrieval
	return nil
}
